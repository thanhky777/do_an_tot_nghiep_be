﻿using BackEnd.Donmain.Models;
using BackEnd.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using System;

namespace BackEnd.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MenuController : ControllerBase
    {
        private readonly IMenuService _menuService;
        public MenuController(IMenuService menuservice)
        {
            _menuService = menuservice;
        }

        [HttpGet]
        [Route("List")]
        public async Task<IActionResult> ListMenu()
        {
            var menus = await _menuService.GetList();

            return Ok(menus);
        }

        [HttpGet]
        [Route("GetById/{id}")]
        public async Task<IActionResult> GetById(int id)
        {
            var menu = await _menuService.GetById(id);
            return Ok(menu);
        }

        [HttpPost]
        [Route("Store")]
        public async Task<IActionResult> StoreMenu(Menu menu)
        {
            await _menuService.Create(menu);
            return Ok();
        }

        [HttpPut]
        [Route("Update/{id}")]
        public async Task<IActionResult> UpdateMenu(int id, Menu menu)
        {
            var mn = await _menuService.GetById(id);
            mn.Name = menu.Name;
            mn.Link = menu.Link;
            mn.Description = menu.Description;
            mn.Status   = menu.Status;
            mn.Position = menu.Position;
            await _menuService.Update(mn);
            return Ok(mn);
        }

        [HttpDelete]
        [Route("Delete/{id}")]
        public async Task<IActionResult> DeleteMenu(int id)
        {
            try
            {
                await _menuService.Delete(id);
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }
    }
}
