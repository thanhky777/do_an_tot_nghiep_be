﻿using BackEnd.Donmain.Models;
using BackEnd.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using System;

namespace BackEnd.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DonHangController : ControllerBase
    {
        private readonly IDonHangService _donhangService;
        public DonHangController(IDonHangService donhangservice)
        {
            _donhangService = donhangservice;
        }

        [HttpGet]
        [Route("List")]
        public async Task<IActionResult> ListDonHang()
        {
            var donhangs = await _donhangService.GetList();

            return Ok(donhangs);
        }

        [HttpGet]
        [Route("GetById/{id}")]
        public async Task<IActionResult> GetById(int id)
        {
            var donhang = await _donhangService.GetById(id);
            return Ok(donhang);
        }

        [HttpPost]
        [Route("Store")]
        public async Task<IActionResult> StoreDonHang(DonHang donhang)
        {
            await _donhangService.Create(donhang);
            return Ok();
        }

        [HttpPut]
        [Route("Update/{id}")]
        public async Task<IActionResult> UpdateDonHang(int id, DonHang donhang)
        {
            var dh = await _donhangService.GetById(id);
            dh.Status = donhang.Status;
            dh.OrderDate = donhang.OrderDate;
            dh.Discount = donhang.Discount;
            dh.AccountID = donhang.AccountID;
            await _donhangService.Update(dh);
            return Ok(dh);
        }

        [HttpDelete]
        [Route("Delete/{id}")]
        public async Task<IActionResult> DeleteDonHang(int id)
        {
            try
            {
                await _donhangService.Delete(id);
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }
    }
}
