﻿using BackEnd.Donmain.Models;
using BackEnd.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using System;

namespace BackEnd.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DanhMucSanPhamController : ControllerBase
    {
        private readonly IDanhMucSanPhamService _danhmucsanphamService;
        public DanhMucSanPhamController(IDanhMucSanPhamService danhmucsanphamservice)
        {
            _danhmucsanphamService = danhmucsanphamservice;
        }

        [HttpGet]
        [Route("List")]
        public async Task<IActionResult> ListDanhMucSanPham()
        {
            var danhmucsanphams = await _danhmucsanphamService.GetList();

            return Ok(danhmucsanphams);
        }

        [HttpGet]
        [Route("GetById/{id}")]
        public async Task<IActionResult> GetById(int id)
        {
            var danhmucsanpham = await _danhmucsanphamService.GetById(id);
            return Ok(danhmucsanpham);
        }

        [HttpPost]
        [Route("Store")]
        public async Task<IActionResult> StoreDanhMucSanPham(DanhMucSanPham danhmucsanpham)
        {
            await _danhmucsanphamService.Create(danhmucsanpham);
            return Ok();
        }

        [HttpPut]
        [Route("Update/{id}")]
        public async Task<IActionResult> UpdateDanhMucSanPham(int id, DanhMucSanPham danhmucsanpham)
        {
            var dmsp = await _danhmucsanphamService.GetById(id);
            dmsp.Name = danhmucsanpham.Name;
            dmsp.SeoTitle = danhmucsanpham.SeoTitle;
            dmsp.Status = danhmucsanpham.Status;
            dmsp.ParentID = danhmucsanpham.ParentID;
            dmsp.CreatedDate = danhmucsanpham.CreatedDate;
            dmsp.UpdatedDate = danhmucsanpham.UpdatedDate;
            await _danhmucsanphamService.Update(dmsp);
            return Ok(dmsp);
        }

        [HttpDelete]
        [Route("Delete/{id}")]
        public async Task<IActionResult> DeleteDanhMucSanPham(int id)
        {
            try
            {
                await _danhmucsanphamService.Delete(id);
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }
    }
}
