﻿using BackEnd.Donmain.Models;
using BackEnd.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using System;

namespace BackEnd.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BinhLuanSanPhamController : ControllerBase
    {
        public readonly IBinhLuanSanPhamService _binhluansanphamService;
        public BinhLuanSanPhamController(IBinhLuanSanPhamService binhluansanphamService)
        {
            _binhluansanphamService = binhluansanphamService;
        }

        [HttpGet("List")]
        public async Task<IActionResult> ListBinhLuanSanPham()
        {
            var binhluansanphams = await _binhluansanphamService.GetList();

            return Ok(binhluansanphams);
        }

        [HttpGet]
        [Route("GetById/{id}")]
        public async Task<IActionResult> GetById(int id)
        {
            var binhluansanpham = await _binhluansanphamService.GetById(id);

            return Ok(binhluansanpham);
        }

        [HttpPost("Store")]
        public async Task<IActionResult> Store(BinhLuanSanPham binhluansanpham)
        {
            await _binhluansanphamService.Create(binhluansanpham);
            return Ok();
        }

        [HttpPut]
        [Route("Update/{id}")]
        public async Task<IActionResult> UpdateBinhLuanSanPham(int id, BinhLuanSanPham binhluansanpham)
        {
            var blsp = await _binhluansanphamService.GetById(id);
            blsp.Name = binhluansanpham.Name;
            blsp.Email = binhluansanpham.Email;
            blsp.Detail = binhluansanpham.Detail;
            blsp.Status = binhluansanpham.Status;
            blsp.CreatedDate = binhluansanpham.CreatedDate;
            blsp.UpdatedDate = binhluansanpham.UpdatedDate;
            blsp.ProductID = binhluansanpham.ProductID;
            await _binhluansanphamService.Update(binhluansanpham);
            return Ok(blsp);
        }

        [HttpDelete]
        [Route("Delete/{id}")]
        public async Task<IActionResult> DeleteBinhLuanSanPham(int id)
        {
            try
            {
                await _binhluansanphamService.Delete(id);
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }
    }
}
