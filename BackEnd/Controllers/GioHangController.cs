﻿using BackEnd.Donmain.Models;
using BackEnd.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using System;

namespace BackEnd.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class GioHangController : ControllerBase
    {
        private readonly IGioHangService _giohangService;
        public GioHangController(IGioHangService giohangservice)
        {
            _giohangService = giohangservice;
        }

        [HttpGet]
        [Route("List")]
        public async Task<IActionResult> ListDanhMucSanPham()
        {
            var giohangs = await _giohangService.GetList();

            return Ok(giohangs);
        }

        [HttpGet]
        [Route("GetById/{id}")]
        public async Task<IActionResult> GetById(int id)
        {
            var giohang = await _giohangService.GetById(id);
            return Ok(giohang);
        }

        [HttpPost]
        [Route("Store")]
        public async Task<IActionResult> StoreGioHang(GioHang giohang)
        {
            await _giohangService.Create(giohang);
            return Ok();
        }

        [HttpPut]
        [Route("Update/{id}")]
        public async Task<IActionResult> UpdateDanhMucSanPham(int id, GioHang giohang)
        {
            var gh = await _giohangService.GetById(id);
            gh.ProductName = giohang.ProductName;
            gh.AccountID = giohang.AccountID;
            gh.ProductID = giohang.ProductID;
            gh.Image = giohang.Image;
            gh.Quantity = giohang.Quantity;
            gh.Price = giohang.Price;
            await _giohangService.Update(gh);
            return Ok(gh);
        }

        [HttpDelete]
        [Route("Delete/{id}")]
        public async Task<IActionResult> DeleteGioHang(int id)
        {
            try
            {
                await _giohangService.Delete(id);
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }

        [HttpGet]
        [Route("GetByIdString/{keyword}")]
        public async Task<IActionResult> GetByIdString(int keyword)
        {
            var khachhang = await _giohangService.Search(keyword);

            return Ok(khachhang);
        }

        [HttpGet]
        [Route("TimKiemAccount/{id}")]
        public async Task<IActionResult> TimKiemAccountID(int id)
        {
            var khachhang = await _giohangService.TimKiemAccountID(id);

            return Ok(khachhang);
        }

        [HttpGet]
        [Route("TimKiemAccountAndProduct/{accountID}/{productID}")]
        public async Task<IActionResult> TimKiemAccountAndProduct(int accountID, int productID)
        {
            var khachhang = await _giohangService.TimKiemAccountAndProduct(accountID, productID);

            return Ok(khachhang);
        }

        [HttpDelete]
        [Route("RemoveRange/{accountID}")]
        public async Task<IActionResult> RemoveRangeAccountAndProduct(int accountID)
        {
            var khachhang = await _giohangService.RemoveRangeAccountAndProduct(accountID);

            return Ok(khachhang);
        }
    }
}
