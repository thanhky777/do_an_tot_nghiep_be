﻿using BackEnd.Donmain.Models;
using BackEnd.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using System;

namespace BackEnd.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ChiTietDonHangController : ControllerBase
    {
        private readonly IChiTietDonHangService _chitietdonhangService;
        public ChiTietDonHangController(IChiTietDonHangService chitietdonhangservice)
        {
            _chitietdonhangService = chitietdonhangservice;
        }

        [HttpGet]
        [Route("List")]
        public async Task<IActionResult> ListChiTietDonHang()
        {
            var chitietdonhangs = await _chitietdonhangService.GetList();

            return Ok(chitietdonhangs);
        }

        [HttpGet]
        [Route("GetById/{id}")]
        public async Task<IActionResult> GetById(int id)
        {
            var chitietdonhang = await _chitietdonhangService.GetById(id);
            return Ok(chitietdonhang);
        }

        [HttpPost]
        [Route("Store")]
        public async Task<IActionResult> StoreChiTietDonHang(ChiTietDonHang chitietdonhang)
        {
            await _chitietdonhangService.Create(chitietdonhang);
            return Ok();
        }

        [HttpPut]
        [Route("Update/{id}")]
        public async Task<IActionResult> UpdateChiTietDonHang(int id, ChiTietDonHang chitietdonhang)
        {
            var ctdh = await _chitietdonhangService.GetById(id);
            ctdh.OrderID = chitietdonhang.OrderID;
            ctdh.ProductID = chitietdonhang.ProductID;
            ctdh.ProductName = chitietdonhang.ProductName;
            ctdh.Price = chitietdonhang.Price;
            ctdh.Quantity = chitietdonhang.Quantity;
            await _chitietdonhangService.Update(ctdh);
            return Ok(ctdh);
        }

        [HttpDelete]
        [Route("Delete/{id}")]
        public async Task<IActionResult> DeleteChiTietDonHang(int id)
        {
            try
            {
                await _chitietdonhangService.Delete(id);
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }

        [HttpDelete]
        [Route("RemoveListChiTietDonHang/{orderID}")]
        public async Task<IActionResult> RemoveListChiTietDonHang(int orderID)
        {
            var donhang = await _chitietdonhangService.RemoveListOrder(orderID);

            return Ok(donhang);
        }

        [HttpGet]
        [Route("DanhSachOrderID/{orderID}")]
        public async Task<IActionResult> DanhSachOrdeID(int orderID)
        {
            var donhangs = await _chitietdonhangService.ListOrderID(orderID);

            return Ok(donhangs);
        }
    }
}
