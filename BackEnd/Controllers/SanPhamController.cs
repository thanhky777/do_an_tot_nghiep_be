﻿using BackEnd.Donmain.Models;
using BackEnd.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using System;
using BackEnd.Helpper;
using System.IO;

namespace BackEnd.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SanPhamController : ControllerBase
    {
        private readonly ISanPhamService _sanphamService;
        public SanPhamController(ISanPhamService sanphamservice)
        {
            _sanphamService = sanphamservice;
        }

        [HttpGet("List")]
        public async Task<IActionResult> ListSanPham()
        {
            var sanphams = await _sanphamService.GetList();

            return Ok(sanphams);
        }

        [HttpGet]
        [Route("GetById/{id}")]
        public async Task<IActionResult> GetById(int id)
        {
            var sanpham = await _sanphamService.GetById(id);

            return Ok(sanpham);
        }



        [HttpPost("Store")]
        public async Task<IActionResult> Strore(SanPham sanpham)
        {
            await _sanphamService.Create(sanpham);
            return Ok();
        }

        [HttpPost("Store-image")]
        public async Task<IActionResult> StroreImage([FromForm] SanPhamImage sanphamimage)
        {
            var sanpham = new SanPham
            {
                Name = sanphamimage.Name,
                SeoTitle = sanphamimage.SeoTitle,
                Status = (int)sanphamimage.Status,
                ListImages = sanphamimage.ListImages,
                Price = (int)sanphamimage.Price,
                PromotionPrice = (int)sanphamimage.PromotionPrice,
                Quantity = (int)sanphamimage.Quantity,
                Warranty = (int)sanphamimage.Warranty,
                Hot = sanphamimage.Hot,
                Description = sanphamimage.Description,
                Detail = sanphamimage.Detail,
                CateID = sanphamimage.CateID,
                BrandID = sanphamimage.BrandID,
                SupplierID = sanphamimage.SupplierID,
                CreateDate = sanphamimage.CreateDate,
                UpdateDate = sanphamimage.UpdateDate,
            };
            if (sanphamimage.fileUpload.Length > 0)
            {
                sanphamimage.Name = Utilities.ToTitleCase(sanphamimage.Name);
                string extension = Path.GetExtension(sanphamimage.fileUpload.FileName);
                string fileName = Utilities.SEOUrl(sanphamimage.Name) + extension;
                sanpham.Image = await Utilities.UploadFile(sanphamimage.fileUpload, @"sanphams", fileName.ToLower());
            }
            else
            {
                sanpham.Image = "default.jpg";
            }
            await _sanphamService.Create(sanpham);
            return Ok();
        }

        [HttpPut]
        [Route("Update/{id}")]
        public async Task<IActionResult> UpdateSanPham(int id, [FromForm] SanPhamImage sanphamimage)
        {
            var sp = await _sanphamService.GetById(id);
            sp.Name = sanphamimage.Name;
            sp.SeoTitle = sanphamimage.SeoTitle;
            sp.Status = (int)sanphamimage.Status;
            sp.ListImages = sanphamimage.ListImages;
            sp.Price = (int)sanphamimage.Price;
            sp.PromotionPrice = (int)sanphamimage.PromotionPrice;
            sp.Quantity = (int)sanphamimage.Quantity;
            sp.Warranty = (int)sanphamimage.Warranty;
            sp.Hot = sanphamimage.Hot;
            sp.Description = sanphamimage.Description;
            sp.Detail = sanphamimage.Detail;
            sp.UpdateDate = sanphamimage.UpdateDate;
            if ( sanphamimage.fileUpload != null && sanphamimage.fileUpload.Length > 0)            
            {
                string fileName;
                if (!string.IsNullOrEmpty(sanphamimage.Name))
                {
                    sanphamimage.Name = Utilities.ToTitleCase(sanphamimage.Name);
                    string extension = Path.GetExtension(sanphamimage.fileUpload.FileName);
                    fileName = Utilities.SEOUrl(sanphamimage.Name) + extension;

                }
                else
                {
                    fileName = Path.GetFileName(sp.Image);
                }
                // Xoa anh cu
                if (!string.IsNullOrEmpty(sp.Image))
                {
                    string filePath = Path.Combine(@"sanphams", sp.Image);
                    if(System.IO.File.Exists(filePath))
                    {
                        System.IO.File.Delete(filePath);
                    }
                }
                sp.Image = await Utilities.UploadFile(sanphamimage.fileUpload, @"sanphams", fileName.ToLower());
            }
            await _sanphamService.Update(sp);
            return Ok(sp);
        }

        [HttpDelete]
        [Route("Delete/{id}")]
        public async Task<IActionResult> DeleteSanPham(int id)
        {
            try
            {
                await _sanphamService.Delete(id);
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }
        [HttpGet]
        [Route("GetImageByPath")]
        public IActionResult Get(string path)
        {
            if (!System.IO.File.Exists(path))
            {
                return NotFound();
            }

            return File(System.IO.File.ReadAllBytes(path), "image/*");
        }

        [HttpGet]
        [Route("TimKiemTheoDanhMucSanPham/{cateID}")]
        public async Task<IActionResult> TimKiemTheoDanhMucSanPham(int cateID)
        {
            var sanpham = await _sanphamService.TimKiemTheoDanhMucSanPham(cateID);

            return Ok(sanpham);
        }

        [HttpGet]
        [Route("TimKiemTheoHangSanPham/{brandID}")]
        public async Task<IActionResult> TimKiemTheoHangSanPham(int brandID)
        {
            var sanpham = await _sanphamService.TimKiemTheoHangSanPham(brandID);

            return Ok(sanpham);
        }

        [HttpGet]
        [Route("SapXepSanPhamAZTenSP")]
        public async Task<IActionResult> SapXepSanPhamAZTenSP()
        {
            var sanpham = await _sanphamService.SapXepSanPhamAZTenSP();

            return Ok(sanpham);
        }

        [HttpGet]
        [Route("SapXepSanPhamZATenSP")]
        public async Task<IActionResult> SapXepSanPhamZATenSP()
        {
            var sanpham = await _sanphamService.SapXepSanPhamZATenSP();

            return Ok(sanpham);
        }

        [HttpGet]
        [Route("ListSanPhamTrangIndex")]
        public async Task<IActionResult> ListSanPhamTrangIndex()
        {
            var sanpham = await _sanphamService.ListSanPhamTrangIndex();

            return Ok(sanpham);
        }
    }
}
