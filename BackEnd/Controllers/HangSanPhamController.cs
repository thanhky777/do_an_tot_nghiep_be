﻿using BackEnd.Donmain.Models;
using BackEnd.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using System;

namespace BackEnd.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class HangSanPhamController : ControllerBase
    {
        private readonly IHangSanPhamService _hangsanphamService;
        public HangSanPhamController(IHangSanPhamService hangsanphamservice)
        {
            _hangsanphamService = hangsanphamservice;
        }

        [HttpGet]
        [Route("List")]
        public async Task<IActionResult> ListHangSanPham()
        {
            var hangsanphams = await _hangsanphamService.GetList();

            return Ok(hangsanphams);
        }

        [HttpGet]
        [Route("GetById/{id}")]
        public async Task<IActionResult> GetById(int id)
        {
            var hangsanpham = await _hangsanphamService.GetById(id);

            return Ok(hangsanpham);
        }

        [HttpPost]
        [Route("Store")]
        public async Task<IActionResult> StoreHangSanPham(HangSanPham hangsanpham)
        {
            await _hangsanphamService.Create(hangsanpham);
            return Ok();
        }

        [HttpPut]
        [Route("Update/{id}")]
        public async Task<IActionResult> UpdateHangSanPham(int id, HangSanPham hangsanpham)
        {
            var hsp = await _hangsanphamService.GetById(id);
            hsp.Name = hangsanpham.Name;
            await _hangsanphamService.Update(hsp);
            return Ok(hsp);
        }

        [HttpDelete]
        [Route("Delete/{id}")]
        public async Task<IActionResult> DeleteHangSanPham(int id)
        {
            try
            {
                await _hangsanphamService.Delete(id);
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }
    }
}
