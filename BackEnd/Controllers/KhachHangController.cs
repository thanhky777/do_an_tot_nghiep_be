﻿using BackEnd.Donmain.Models;
using BackEnd.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using System;
using System.IO;
using BackEnd.Helpper;
using Microsoft.AspNetCore.Hosting.Server;
using System.Collections.Generic;

namespace BackEnd.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class KhachHangController : ControllerBase
    {
        private readonly IKhachHangService _khachhangService;
        public KhachHangController(IKhachHangService khachhangservice)
        {
            _khachhangService = khachhangservice;
        }

        [HttpGet]
        [Route("List")]
        public async Task<IActionResult> ListKhachHang()
        {
            var khachhangs = await _khachhangService.GetList();

            return Ok(khachhangs);
        }

        [HttpGet]
        [Route("GetById/{id}")]
        public async Task<IActionResult> GetById(int id)
        {
            var khachhang = await _khachhangService.GetById(id);
            return Ok(khachhang);
        }

        [HttpGet]
        [Route("GetByIdString/{keyword}")]
        public async Task<IActionResult> GetByIdString(string keyword)
        {
            var khachhang = await _khachhangService.Search(keyword);

            return Ok(khachhang);
        }

        [HttpPost]
        [Route("Store")]
        public async Task<IActionResult> StoreKhachHang(KhachHang khachhang)
        {
            await _khachhangService.Create(khachhang);
            return Ok();
        }
        [HttpPost]
        [Route("Store-image")]
        public async Task<IActionResult> StoreImage([FromForm] KhachHangImage khachhangimage)
        {
            var khachhang = new KhachHang
            {
                AccountID = khachhangimage.AccountID,
                Name = khachhangimage.Name,
                Password = khachhangimage.Password,
                Email = khachhangimage.Email,
                Phone = khachhangimage.Phone,
                Sex = khachhangimage.Sex,
                Age = khachhangimage.Age,
                Address = khachhangimage.Address
            };
            if (khachhangimage.fileUpload.Length > 0)
            {
                khachhangimage.Name = Utilities.ToTitleCase(khachhangimage.Name);
                string extension = Path.GetExtension(khachhangimage.fileUpload.FileName);
                string fileName = Utilities.SEOUrl(khachhangimage.Name) + extension;
                khachhang.Avatar = await Utilities.UploadFile(khachhangimage.fileUpload, @"khachhangs", fileName.ToLower());
            }
            else
            {
                khachhang.Avatar = "default.jpg";
            }
            await _khachhangService.Create(khachhang);
            return Ok();
        }

        [HttpPut]
        [Route("Update/{id}")]
        public async Task<IActionResult> UpdateKhachHang(int id, KhachHang khachhang)
        {
            var kh = await _khachhangService.GetById(id);
            kh.Name = khachhang.Name;
            kh.Password = khachhang.Password;
            kh.Email = khachhang.Email;
            kh.Phone    = khachhang.Phone;
            kh.Sex = khachhang.Sex;
            kh.Age = khachhang.Age;
            kh.Avatar = khachhang.Avatar;
            kh.Address = khachhang.Address;

            await _khachhangService.Update(kh);
            return Ok(kh);
        }

        [HttpPut]
        [Route("UpdateImage/{id}")]
        public async Task<IActionResult> UpdateImageKhachHang(int id, [FromForm] KhachHangImage khachhangimage)
        {
            var kh = await _khachhangService.GetById(id);
            kh.Name = khachhangimage.Name;
            kh.Password = khachhangimage.Password;
            kh.Email = khachhangimage.Email;
            kh.Phone = khachhangimage.Phone;
            kh.Sex = khachhangimage.Sex;
            kh.Age = khachhangimage.Age;
            kh.Address = khachhangimage.Address;

            if (khachhangimage.fileUpload != null && khachhangimage.fileUpload.Length > 0)
            {
                string fileName;

                if (!string.IsNullOrEmpty(khachhangimage.Name))
                {
                    khachhangimage.Name = Utilities.ToTitleCase(khachhangimage.Name);
                    string extension = Path.GetExtension(khachhangimage.fileUpload.FileName);
                    fileName = Utilities.SEOUrl(khachhangimage.Name) + extension;
                }
                else
                {
                    fileName = Path.GetFileName(kh.Avatar);
                }
                // Xóa ảnh cũ
                if (!string.IsNullOrEmpty(kh.Avatar))
                {
                    string filePath = Path.Combine(@"khachhangs", kh.Avatar);
                    if (System.IO.File.Exists(filePath))
                    {
                        System.IO.File.Delete(filePath);
                    }
                }

                kh.Avatar = await Utilities.UploadFile(khachhangimage.fileUpload, @"khachhangs", fileName.ToLower());
            }

            await _khachhangService.Update(kh);
            return Ok(kh);
        }


        [HttpDelete]
        [Route("Delete/{id}")]
        public async Task<IActionResult> DeleteKhachHang(int id)
        {
            try
            {
                await _khachhangService.Delete(id);
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }
    }
}
