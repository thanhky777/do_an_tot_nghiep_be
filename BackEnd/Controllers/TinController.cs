﻿using BackEnd.Donmain.Models;
using BackEnd.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using System;
using BackEnd.Helpper;
using System.IO;

namespace BackEnd.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TinController : ControllerBase
    {
        private readonly ITinService _tinService;
        public TinController(ITinService tinservice)
        {
            _tinService = tinservice;
        }

        [HttpGet]
        [Route("List")]
        public async Task<IActionResult> ListTin()
        {
            var tins = await _tinService.GetList();

            return Ok(tins);
        }

        [HttpGet]
        [Route("GetById/{id}")]
        public async Task<IActionResult> GetById(int id)
        {
            var tin = await _tinService.GetById(id);
            return Ok(tin);
        }

        [HttpPost]
        [Route("Store")]
        public async Task<IActionResult> StoreTin(Tin tin)
        {
            var path = "D:\\University_documents_DaiNam\\Year4\\Semester2\\DoAnTotNghiep2023\\BackEnd\\FolderNewsImage\\";
            tin.Image = String.Concat(path, tin.Image);
            await _tinService.Create(tin);
            return Ok();
        }

        [HttpPost]
        [Route("Store-image")]
        public async Task<IActionResult> StoreTinImage([FromForm] TinImage tinImage)
        {
            var tin = new Tin
            {
                Name = tinImage.Name,
                SeoTitle = tinImage.SeoTitle,
                Status = tinImage.Status,
                Hot = tinImage.Hot,
                Description = tinImage.Description,
                Detail = tinImage.Detail,
                CreatedDate = tinImage.CreatedDate,
                UpdatedDate = tinImage.UpdatedDate,
                CatePostID = tinImage.CatePostID
            };
            if (tinImage.fileUpload.Length > 0)
            {
                tinImage.Name = Utilities.ToTitleCase(tinImage.Name);
                string extension = Path.GetExtension(tinImage.fileUpload.FileName);
                string fileName = Utilities.SEOUrl(tinImage.Name) + extension;
                tin.Image = await Utilities.UploadFile(tinImage.fileUpload, @"tins", fileName.ToLower());
            }
            else
            {
                tin.Image = "default.jpg";
            }
            await _tinService.Create(tin);
            return Ok();
        }

        [HttpPut]
        [Route("Update/{id}")]
        public async Task<IActionResult> UpdateTin(int id, Tin tin)
        {
            var t = await _tinService.GetById(id);
            t.Name = tin.Name;
            t.SeoTitle = tin.SeoTitle;
            t.Status = tin.Status;
            t.Image = tin.Image;
            t.Hot = tin.Hot;
            t.Description = tin.Description;
            t.Detail = tin.Detail;
            t.CreatedDate = tin.CreatedDate;
            t.UpdatedDate = tin.UpdatedDate;
            t.CatePostID = tin.CatePostID;
            await _tinService.Update(t);
            return Ok(t);
        }

        [HttpDelete]
        [Route("Delete/{id}")]
        public async Task<IActionResult> DeleteTin(int id)
        {
            try
            {
                await _tinService.Delete(id);
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }

        [HttpPost]
        [Route("Upload")]
        public IActionResult Upload(IFormFile formFile)
        {
            try
            {
                _tinService.UploadFile(formFile);

                return Ok(new { length = formFile, name = formFile.FileName });
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpGet]
        [Route("GetImageByPath")]
        public IActionResult Get(string path)
        {
            if (!System.IO.File.Exists(path))
            {
                return NotFound();
            }

            return File(System.IO.File.ReadAllBytes(path), "image/*");
        }
    }
}
