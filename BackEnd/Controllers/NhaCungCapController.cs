﻿using BackEnd.Donmain.Models;
using BackEnd.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using System;

namespace BackEnd.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class NhaCungCapController : ControllerBase
    {
        private readonly INhaCungCapService _nhacungcapService;
        public NhaCungCapController(INhaCungCapService nhacungcapservice)
        {
            _nhacungcapService = nhacungcapservice;
        }

        [HttpGet]
        [Route("List")]
        public async Task<IActionResult> ListNhaCungCap()
        {
            var nhacungcaps = await _nhacungcapService.GetList();

            return Ok(nhacungcaps);
        }

        [HttpGet]
        [Route("GetById/{id}")]
        public async Task<IActionResult> GetById(int id)
        {
            var nhacungcap = await _nhacungcapService.GetById(id);

            return Ok(nhacungcap);
        }

        [HttpPost]
        [Route("Store")]
        public async Task<IActionResult> StoreNhaCungCap(NhaCungCap nhacungcap)
        {
            await _nhacungcapService.Create(nhacungcap);
            return Ok();
        }

        [HttpPut]
        [Route("Update/{id}")]
        public async Task<IActionResult> UpdateNhaCungCap(int id, NhaCungCap nhacungcap)
        {
            var ncc = await _nhacungcapService.GetById(id);
            ncc.Name = nhacungcap.Name;
            ncc.Email = nhacungcap.Email;
            ncc.Phone = nhacungcap.Phone;
            ncc.Address = nhacungcap.Address;
            await _nhacungcapService.Update(ncc);
            return Ok(ncc);
        }

        [HttpDelete]
        [Route("Delete/{id}")]
        public async Task<IActionResult> DeleteNhaCungCap(int id)
        {
            try
            {
                await _nhacungcapService.Delete(id);
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }
    }
}
