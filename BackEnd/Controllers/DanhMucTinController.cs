﻿using BackEnd.Donmain.Models;
using BackEnd.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using System;

namespace BackEnd.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DanhMucTinController : ControllerBase
    {
        private readonly IDanhMucTinService _danhmuctinService;
        public DanhMucTinController(IDanhMucTinService danhmuctinservice)
        {
            _danhmuctinService = danhmuctinservice;
        }

        [HttpGet]
        [Route("List")]
        public async Task<IActionResult> ListDanhMucTin()
        {
            var danhmuctins = await _danhmuctinService.GetList();

            return Ok(danhmuctins);
        }

        [HttpGet]
        [Route("GetById/{id}")]
        public async Task<IActionResult> GetById(int id)
        {
            var danhmuctin  = await _danhmuctinService.GetById(id);
            return Ok(danhmuctin);
        }

        [HttpPost]
        [Route("Store")]
        public async Task<IActionResult> StoreDanhMucTin(DanhMucTin danhmuctin)
        {
            await _danhmuctinService.Create(danhmuctin);
            return Ok();
        }

        [HttpPut]
        [Route("Update/{id}")]
        public async Task<IActionResult> UpdateDanhMucTin(int id, DanhMucTin danhmuctin)
        {
            var dmt = await _danhmuctinService.GetById(id);
            dmt.Name = danhmuctin.Name;
            dmt.Status = danhmuctin.Status;
            dmt.SeoTitle=danhmuctin.SeoTitle;
            dmt.ParentID = danhmuctin.ParentID;
            dmt.CreatedDate = danhmuctin.CreatedDate;
            dmt.UpdatedDate = danhmuctin.UpdatedDate;
            await _danhmuctinService.Update(dmt);
            return Ok(dmt);
        }

        [HttpDelete]
        [Route("Delete/{id}")]
        public async Task<IActionResult> DeleteDanhMucTin(int id)
        {
            try
            {
                await _danhmuctinService.Delete(id);
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }
    }
}
