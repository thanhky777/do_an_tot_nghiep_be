﻿using BackEnd.Donmain.Models;
using BackEnd.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using System;

namespace BackEnd.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class NhapHangController : ControllerBase
    {
        private readonly INhapHangService _nhaphangService;
        public NhapHangController(INhapHangService nhaphangservice)
        {
            _nhaphangService = nhaphangservice;
        }

        [HttpGet]
        [Route("List")]
        public async Task<IActionResult> ListNhapHang()
        {
            var nhaphangs = await _nhaphangService.GetList();

            return Ok(nhaphangs);
        }

        [HttpGet]
        [Route("GetById/{id}")]
        public async Task<IActionResult> GetById(int id)
        {
            var nhaphang = await _nhaphangService.GetById(id);

            return Ok(nhaphang);
        }

        [HttpPost]
        [Route("Store")]
        public async Task<IActionResult> StoreNhapHang(NhapHang nhaphang)
        {
            await _nhaphangService.Create(nhaphang);
            return Ok();
        }

        [HttpPut]
        [Route("Update/{id}")]
        public async Task<IActionResult> UpdateNhapHang(int id, NhapHang nhaphang)
        {
            var nh = await _nhaphangService.GetById(id);
            nh.Status = nhaphang.Status;
            //nh.CreateDate = DateTime.Now;
            nh.CreateDate = nhaphang.CreateDate;
            nh.UpdateDate = nhaphang.UpdateDate;
            await _nhaphangService.Update(nh);
            return Ok(nh);
        }

        [HttpDelete]
        [Route("Delete/{id}")]
        public async Task<IActionResult> DeleteNhapHang(int id)
        {
            try
            {
                await _nhaphangService.Delete(id);
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }
    }
}
