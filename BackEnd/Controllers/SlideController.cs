﻿using BackEnd.Donmain.Models;
using BackEnd.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using System;

namespace BackEnd.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SlideController : ControllerBase
    {
        private readonly ISlideService _slideService;
        public SlideController(ISlideService slideservice)
        {
            _slideService = slideservice;
        }

        [HttpGet]
        [Route("List")]
        public async Task<IActionResult> ListSlide()
        {
            var slides = await _slideService.GetList();

            return Ok(slides);
        }

        [HttpGet]
        [Route("GetById/{id}")]
        public async Task<IActionResult> GetById(int id)
        {
            var slide = await _slideService.GetById(id);
            return Ok(slide);
        }

        [HttpPost]
        [Route("Store")]
        public async Task<IActionResult> StoreSlide(Slide slide)
        {
            await _slideService.Create(slide);
            return Ok();
        }

        [HttpPut]
        [Route("Update/{id}")]
        public async Task<IActionResult> UpdateSlide(int id, Slide slide)
        {
            var sl = await _slideService.GetById(id);
            sl.Name = slide.Name;
            sl.Image = slide.Image;
            sl.Status = slide.Status;
            await _slideService.Update(sl);
            return Ok(sl);
        }

        [HttpDelete]
        [Route("Delete/{id}")]
        public async Task<IActionResult> DeleteSlide(int id)
        {
            try
            {
                await _slideService.Delete(id);
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }
    }
}
