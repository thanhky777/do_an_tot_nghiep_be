﻿using BackEnd.Donmain.Models;
using BackEnd.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using System;

namespace BackEnd.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ChiTietNhapHangController : ControllerBase
    {
        private readonly IChiTietNhapHangService _chitietnhaphangService;
        public ChiTietNhapHangController(IChiTietNhapHangService chitietnhaphangservice)
        {
            _chitietnhaphangService = chitietnhaphangservice;
        }

        [HttpGet]
        [Route("List")]
        public async Task<IActionResult> ListChiTietNhapHang()
        {
            var chitietnhaphangs = await _chitietnhaphangService.GetList();

            return Ok(chitietnhaphangs);
        }

        [HttpGet]
        [Route("GetById/{id}")]
        public async Task<IActionResult> GetById(int id)
        {
            var chitietnhaphang = await _chitietnhaphangService.GetById(id);

            return Ok(chitietnhaphang);
        }

        [HttpPost]
        [Route("Store")]
        public async Task<IActionResult> StoreChiTietNhapHang(ChiTietNhapHang chitietnhaphang)
        {
            await _chitietnhaphangService.Create(chitietnhaphang);
            return Ok();
        }

        [HttpPut]
        [Route("Update/{id}")]
        public async Task<IActionResult> UpdateChiTietNhapHang(int id, ChiTietNhapHang chitietnhaphang)
        {
            var ctnh = await _chitietnhaphangService.GetById(id);
            ctnh.InvoiceID = chitietnhaphang.InvoiceID;
            ctnh.ProductID = chitietnhaphang.ProductID;
            ctnh.ProductName = chitietnhaphang.ProductName;
            ctnh.Quantity = chitietnhaphang.Quantity;
            ctnh.Price = chitietnhaphang.Price;
            return Ok(ctnh);
        }

        [HttpDelete]
        [Route("Delete/{id}")]
        public async Task<IActionResult> DeleteChiTietNhapHang(int id)
        {
            try
            {
                await _chitietnhaphangService.Delete(id);
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }
    }
}
