﻿using BackEnd.Donmain.Models;
using BackEnd.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using System;

namespace BackEnd.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BinhLuanTinController : ControllerBase
    {
        private readonly IBinhLuanTinService _binhluantinService;
        public BinhLuanTinController(IBinhLuanTinService binhluantinservice)
        {
            _binhluantinService = binhluantinservice;
        }

        [HttpGet]
        [Route("List")]
        public async Task<IActionResult> ListBinhLuanTin()
        {
            var binhluantins = await _binhluantinService.GetList();

            return Ok(binhluantins);
        }

        [HttpGet]
        [Route("GetById/{id}")]
        public async Task<IActionResult> GetById(int id)
        {
            var binhluantin = await _binhluantinService.GetById(id);
            return Ok(binhluantin);
        }

        [HttpPost]
        [Route("Store")]
        public async Task<IActionResult> StoreBinhLuanTin(BinhLuanTin binhluantin)
        {
            await _binhluantinService.Create(binhluantin);
            return Ok();
        }

        [HttpPut]
        [Route("Update/{id}")]
        public async Task<IActionResult> UpdateBinhLuanTin(int id, BinhLuanTin binhluantin)
        {
            var blt = await _binhluantinService.GetById(id);
            blt.Name = binhluantin.Name;
            blt.Email = binhluantin.Email;
            blt.Detail = binhluantin.Detail;
            blt.Status  = binhluantin.Status;
            blt.PostID = binhluantin.PostID;
            blt.CreatedDate = binhluantin.CreatedDate;
            blt.UpdatedDate = binhluantin.UpdatedDate;
            await _binhluantinService.Update(blt);
            return Ok(blt);
        }

        [HttpDelete]
        [Route("Delete/{id}")]
        public async Task<IActionResult> DeleteBinhLuanTin(int id)
        {
            try
            {
                await _binhluantinService.Delete(id);
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }
    }
}
