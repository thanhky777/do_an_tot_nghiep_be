using BackEnd.Donmain.Models;
using BackEnd.Repository;
using BackEnd.Services;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.OpenApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BackEnd
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {

            services.AddControllers();
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "BackEnd", Version = "v1" });
            });
            services.AddDbContext<WebAPIDBContext>(options => options.UseSqlServer(Configuration.GetConnectionString("ConnStr")));
            services.AddScoped(typeof(IRepository<>), typeof(Repository<>));
            services.AddScoped<IDanhMucSanPhamService, DanhMucSanPhamService>();
            services.AddScoped<ISanPhamService, SanPhamService>();
            services.AddScoped<IHangSanPhamService, HangSanPhamService>();
            services.AddScoped<INhaCungCapService, NhaCungCapService>();
            services.AddScoped<IBinhLuanSanPhamService, BinhLuanSanPhamService>();
            services.AddScoped<INhapHangService, NhapHangService>();
            services.AddScoped<IChiTietNhapHangService, ChiTietNhapHangService>();
            services.AddScoped<IKhachHangService, KhachHangService>();
            services.AddScoped<IDonHangService, DonHangService>();
            services.AddScoped<IChiTietDonHangService, ChiTietDonHangService>();
            services.AddScoped<IDanhMucTinService, DanhMucTinService>();
            services.AddScoped<ITinService, TinService>();
            services.AddScoped<IBinhLuanTinService, BinhLuanTinService>();
            services.AddScoped<IMenuService, MenuService>();
            services.AddScoped<ISlideService, SlideService>();
            services.AddScoped<IKhachHangRepository<KhachHang>, KhackHanhRepository<KhachHang>>();
            services.AddScoped<IGioHangService, GioHangService>();
            services.AddScoped<IGioHangRepository<GioHang>, GioHangRepository<GioHang>>();
            services.AddScoped<IChiTietDonHangRespository<ChiTietDonHang>, ChiTietDonHangRepository<ChiTietDonHang>>();
            services.AddScoped<ISanPhamRepository<SanPham>, SanPhamRepository<SanPham>>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "BackEnd v1"));
            }

            app.UseCors(x => x
               .AllowAnyMethod()
               .AllowAnyHeader()
               .SetIsOriginAllowed(origin => true) // allow any origin
               .AllowCredentials()); // allow credentials

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
