﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BackEnd.Donmain.Models
{
    public class SanPhamImage
    {
        public int ProductID { get; set; }
        public string Name { get; set; }
        public string? SeoTitle { get; set; }
        public int? Status { get; set; }
        [NotMapped]
        public IFormFile? fileUpload { get; set; }
        public string? ListImages { get; set; }
        public int? Price { get; set; }
        public int? PromotionPrice { get; set; }
        public int? Quantity { get; set; }
        public int? Warranty { get; set; }
        public DateTime Hot { get; set; }
        public string? Description { get; set; }
        public string? Detail { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime UpdateDate { get; set; }
        public int? CateID { get; set; }
        public int? BrandID { get; set; }
        public int? SupplierID { get; set; }
    }
}
