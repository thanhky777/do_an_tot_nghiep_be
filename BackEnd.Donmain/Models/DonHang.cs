﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BackEnd.Donmain.Models
{
    public class DonHang
    {
        public int OrderID { get; set; }
        public DateTime OrderDate { get; set; }
        public int Status { get; set; }
        public int Discount { get; set; }
        public int AccountID { get; set; }
        public KhachHang khachhangs { get; set; }
        public ICollection<ChiTietDonHang>? ChiTietDonHangs { get; set; }
    }
}
