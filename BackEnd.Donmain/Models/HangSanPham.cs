﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BackEnd.Donmain.Models
{
    public class HangSanPham
    {
        public int BrandID { get; set; }
        public string Name { get; set; }
        public ICollection<SanPham>? SanPhams { get; set; }
    }
}
