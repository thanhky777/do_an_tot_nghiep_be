﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BackEnd.Donmain.Models
{
    public class TinImage
    {
        public int PostID { get; set; }
        public string Name { get; set; }
        public string SeoTitle { get; set; }
        public int Status { get; set; }
        [NotMapped]
        public IFormFile fileUpload { get; set; }
        public DateTime Hot { get; set; }
        public string Description { get; set; }
        public string Detail { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime UpdatedDate { get; set; }
        public int CatePostID { get; set; }
    }
}
