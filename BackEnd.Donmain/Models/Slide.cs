﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BackEnd.Donmain.Models
{
    public class Slide
    {
        public int SlideID { get; set; }
        public string Name { get; set; }
        public string Image { get; set; }
        public int Status { get; set; }
    }
}
