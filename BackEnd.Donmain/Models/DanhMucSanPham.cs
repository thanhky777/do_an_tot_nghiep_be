﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BackEnd.Donmain.Models
{
    public class DanhMucSanPham
    {
        public int CateID { get; set; }
        public string Name { get; set; }
        public int Status { get; set; }
        public string SeoTitle { get; set; }
        public int ParentID { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public ICollection<SanPham>? SanPhams { get; set; }
    }
}
