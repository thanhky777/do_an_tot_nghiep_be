﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BackEnd.Donmain.Models
{
    public class KhachHangImage
    {
        public int AccountID { get; set; }
        public string Name { get; set; }
        public string Password { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public int? Sex { get; set; }
        public int? Age { get; set; }
        [NotMapped]
        public IFormFile? fileUpload { get; set; }
        public string? Address { get; set; }
        public ICollection<DonHang>? DonHangs { get; set; }
    }
}
