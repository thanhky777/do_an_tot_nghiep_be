﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BackEnd.Donmain.Models
{
    public class Menu
    {
        public int MenuID { get; set; }
        public string Name { get; set; }
        public string Link { get; set; }
        public string Description { get; set; }
        public int Status { get; set; }
        public int Position { get; set; }
    }
}
