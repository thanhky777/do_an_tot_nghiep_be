﻿using BackEnd.Donmain.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BackEnd.Donmain.EntityMapper
{
    public class SlideMap : IEntityTypeConfiguration<Slide>
    {
        public void Configure(EntityTypeBuilder<Slide> builder)
        {
            builder.ToTable("Slide")
               .HasKey(x => x.SlideID)
               .HasName("pk_slideid");
            builder.Property(x => x.SlideID)
                .HasColumnName("SlideID")
                .HasColumnType("INT");
            builder.Property(x => x.Name)
                .HasColumnName("Name")
                .HasColumnType("NVARCHAR(200)");
            builder.Property(x => x.Image)
                .HasColumnName("Image")
                .HasColumnType("VARCHAR(500)");
            builder.Property(x => x.Status)
                .HasColumnName("Status")
                .HasColumnType("INT");
        }
    }
}
