﻿using BackEnd.Donmain.Models;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BackEnd.Donmain.EntityMapper
{
    public class ChiTietDonHangMap : IEntityTypeConfiguration<ChiTietDonHang>
    {
        public void Configure(EntityTypeBuilder<ChiTietDonHang> builder)
        {
            builder.ToTable("ChiTietDonHang")
                .HasKey(x => x.OrderDetailID)
                .HasName("pk_chitietdonhangid");
            builder.Property(x => x.OrderDetailID)
                .HasColumnName("OrderDetailID")
                .HasColumnType("INT");
            builder.Property(x => x.OrderID)
                .HasColumnName("OrderID")
                .HasColumnType("INT");
            builder.Property(x => x.ProductID)
                .HasColumnName("ProductID")
                .HasColumnType("INT");
            builder.Property(x => x.Price)
                .HasColumnName("Price")
                .HasColumnType("INT");
            builder.Property(x => x.Quantity)
                .HasColumnName("Quantity")
                .HasColumnType("INT");
            builder.HasOne<SanPham>(x => x.sanphams)
                .WithMany(a => a.ChiTietDonHang)
                .HasForeignKey(x => x.ProductID)
                .OnDelete(DeleteBehavior.Cascade);
            builder.HasOne<DonHang>(x => x.donhangs)
                .WithMany(a => a.ChiTietDonHangs)
                .HasForeignKey(x => x.OrderID)
                .OnDelete(DeleteBehavior.Cascade);
        }
    }
}
