﻿using BackEnd.Donmain.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BackEnd.Donmain.EntityMapper
{
    public class GioHangMap : IEntityTypeConfiguration<GioHang>
    {
        public void Configure(EntityTypeBuilder<GioHang> builder)
        {
            builder.ToTable("GioHang")
                .HasKey(x => x.CartID)
                .HasName("pk_giohangid");
            builder.Property(x => x.CartID)
                .HasColumnName("CartID")
                .HasColumnType("INT");
            builder.Property(x => x.ProductName)
                .HasColumnName("ProductName")
                .HasColumnType("NVARCHAR(200)");
            builder.Property(x => x.AccountID)
                .HasColumnName("AccountID")
                .HasColumnType("INT");
            builder.Property(x => x.ProductID)
                .HasColumnName("ProductID")
                .HasColumnType("INT");
            builder.Property(x => x.Quantity)
                .HasColumnName("Quantity")
                .HasColumnType("INT");
            builder.Property(x => x.Price)
                .HasColumnName("Price")
                .HasColumnType("INT");
            builder.Property(x => x.Image)
                .HasColumnName("Image")
                .HasColumnType("NVARCHAR(MAX)");
        }
    }
}
