﻿using BackEnd.Donmain.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BackEnd.Donmain.EntityMapper
{
    public class BinhLuanTinMap : IEntityTypeConfiguration<BinhLuanTin>
    {
        public void Configure(EntityTypeBuilder<BinhLuanTin> builder)
        {
            builder.ToTable("BinhLuanTin")
                .HasKey(x => x.CommentID)
                .HasName("pk_binhluantinid");
            builder.Property(x => x.CommentID)
                .HasColumnName("CommentID")
                .HasColumnType("INT");
            builder.Property(x => x.Name)
                .HasColumnName("Name")
                .HasColumnType("NVARCHAR(200)");
            builder.Property(x => x.Email)
                .HasColumnName("Email")
                .HasColumnType("VARCHAR(200)");
            builder.Property(x => x.Detail)
                .HasColumnName("Detail")
                .HasColumnType("NVARCHAR(500)");
            builder.Property(x => x.Status)
                .HasColumnName("Status")
                .HasColumnType("INT");
            builder.Property(x => x.CreatedDate)
                .HasColumnName("CreatedDate")
                .HasColumnType("DateTime");
            builder.Property(x => x.UpdatedDate)
                .HasColumnName("UpdatedDate")
                .HasColumnType("DateTime");
            builder.Property(x => x.PostID)
                .HasColumnName("PostID")
                .HasColumnType("INT");
            builder.HasOne<Tin>(x => x.tin)
                .WithMany(a => a.BinhLuanTin)
                .HasForeignKey(x => x.PostID)
                .OnDelete(DeleteBehavior.Cascade);
        }
    }
}
