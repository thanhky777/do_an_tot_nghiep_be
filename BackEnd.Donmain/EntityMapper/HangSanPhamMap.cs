﻿using BackEnd.Donmain.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BackEnd.Donmain.EntityMapper
{
    public class HangSanPhamMap : IEntityTypeConfiguration<HangSanPham>
    {
        public void Configure(EntityTypeBuilder<HangSanPham> builder)
        {
            builder.ToTable("HangSanPham")
                .HasKey(x => x.BrandID)
                .HasName("pk_hangsanphamid");
            builder.Property(x => x.BrandID)
                .HasColumnName("BrandID")
                .HasColumnType("INT");
            builder.Property(x => x.Name)
                .HasColumnName("Name")
                .HasColumnType("NVARCHAR(200)");
        }
    }
}
