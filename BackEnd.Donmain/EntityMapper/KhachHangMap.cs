﻿using BackEnd.Donmain.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BackEnd.Donmain.EntityMapper
{
    public class KhachHangMap : IEntityTypeConfiguration<KhachHang>
    {
        public void Configure(EntityTypeBuilder<KhachHang> builder)
        {
            builder.ToTable("KhachHang")
                .HasKey(x => x.AccountID)
                .HasName("pk_khachhangid");
            builder.Property(x => x.AccountID)
                .HasColumnName("CommentID")
                .HasColumnType("INT");
            builder.Property(x => x.Name)
                .HasColumnName("Name")
                .HasColumnType("NVARCHAR(200)");
            builder.Property(x => x.Password)
                .HasColumnName("Password")
                .HasColumnType("NVARCHAR(200)");
            builder.Property(x => x.Email)
                .HasColumnName("Email")
                .HasColumnType("NVARCHAR(200)");
            builder.Property(x => x.Phone)
                .HasColumnName("Phone")
                .HasColumnType("VARCHAR(15)");
            builder.Property(x => x.Age)
                .HasColumnName("Age")
                .HasColumnType("INT")
                .IsRequired(false);
            builder.Property(x => x.Avatar)
                .HasColumnName("Avatar")
                .HasColumnType("NVARCHAR(1000)")
                .IsRequired(false);
            builder.Property(x => x.Address)
                .HasColumnName("Address")
                .HasColumnType("NVARCHAR(200)")
                .IsRequired(false);
        }
    }
}
