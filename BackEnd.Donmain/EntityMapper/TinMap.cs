﻿using BackEnd.Donmain.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BackEnd.Donmain.EntityMapper
{
    public class TinMap : IEntityTypeConfiguration<Tin>
    {
        public void Configure(EntityTypeBuilder<Tin> builder)
        {
            builder.ToTable("Tin")
                .HasKey(x => x.PostID)
                .HasName("pk_tinid");
            builder.Property(x => x.PostID)
                .HasColumnName("PostID")
                .HasColumnType("INT");
            builder.Property(x => x.Name)
                .HasColumnName("Name")
                .HasColumnType("NVARCHAR(200)");
            builder.Property(x => x.SeoTitle)
                .HasColumnName("SeoTitle")
                .HasColumnType("VARCHAR(200)");
            builder.Property(x => x.Status)
                .HasColumnName("Status")
                .HasColumnType("INT");
            builder.Property(x => x.Image)
                .HasColumnName("Image")
                .HasColumnType("NVARCHAR(500)");
            builder.Property(x => x.Hot)
                .HasColumnName("Hot")
                .HasColumnType("DATETIME");
            builder.Property(x => x.Description)
                .HasColumnName("Description")
                .HasColumnType("NVARCHAR(500)");
            builder.Property(x => x.Detail)
                .HasColumnName("Detail")
                .HasColumnType("NVARCHAR(MAX)");
            builder.Property(x => x.CreatedDate)
                .HasColumnName("CreatedDate")
                .HasColumnType("DateTime");
            builder.Property(x => x.UpdatedDate)
                .HasColumnName("UpdatedDate")
                .HasColumnType("DateTime");
            builder.Property(x => x.CatePostID)
                .HasColumnName("CatePostID")
                .HasColumnType("INT");
            builder.HasOne<DanhMucTin>(x => x.danhmuctin)
                .WithMany(a => a.Tin)
                .HasForeignKey(x => x.CatePostID)
                .OnDelete(DeleteBehavior.Cascade);
        }
    }
}
