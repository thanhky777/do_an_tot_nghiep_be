﻿using BackEnd.Donmain.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BackEnd.Donmain.EntityMapper
{
    public class SanPhamMap : IEntityTypeConfiguration<SanPham>
    {
        public void Configure(EntityTypeBuilder<SanPham> builder)
        {
            builder.ToTable("SanPham")
                .HasKey(x => x.ProductID)
                .HasName("pk_sanphamid");
            builder.Property(x => x.ProductID)
                .HasColumnName("ProductID")
                .HasColumnType("INT");
            builder.Property(x => x.Name)
                .HasColumnName("Name")
                .HasColumnType("NVARCHAR(200)");
            builder.Property(x => x.SeoTitle)
                .HasColumnName("SeoTitle")
                .HasColumnType("NVARCHAR(200)");
            builder.Property(x => x.Status)
                .HasColumnName("Status")
                .HasColumnType("INT");
            builder.Property(x => x.Image)
                .HasColumnName("Image")
                .HasColumnType("NVARCHAR(500)");
            builder.Property(x => x.ListImages)
                .HasColumnName("ListImages")
                .HasColumnType("VARCHAR(1000)");
            builder.Property(x => x.Price)
                .HasColumnName("Price")
                .HasColumnType("INT");
            builder.Property(x => x.PromotionPrice)
                .HasColumnName("PromotionPrice")
                .HasColumnType("INT");
            builder.Property(x => x.Quantity)
                .HasColumnName("Quantity")
                .HasColumnType("INT");
            builder.Property(x => x.Warranty)
                .HasColumnName("Warranty")
                .HasColumnType("INT");
            builder.Property(x => x.Hot)
                .HasColumnName("Hot")
                .HasColumnType("DateTime");
            builder.Property(x => x.Description)
                .HasColumnName("Description")
                .HasColumnType("NVARCHAR(500)");
            builder.Property(x => x.Detail)
                .HasColumnName("Detail")
                .HasColumnType("NVARCHAR(MAX)");
            builder.Property(x => x.CreateDate)
                .HasColumnName("CreateDate")
                .HasColumnType("DateTime");
            builder.Property(x => x.UpdateDate)
                .HasColumnName("UpdateDate")
                .HasColumnType("DateTime");
            builder.Property(x => x.CateID)
                .HasColumnName("CateID")
                .HasColumnType("INT");
            builder.Property(x => x.BrandID)
                .HasColumnName("BrandID")
                .HasColumnType("INT");
            builder.Property(x => x.SupplierID)
                .HasColumnName("SupplierID")
                .HasColumnType("INT");
            builder.HasOne<DanhMucSanPham>(x => x.danhmucsanpham)
                .WithMany(a => a.SanPhams)
                .HasForeignKey(x => x.CateID)
                .OnDelete(DeleteBehavior.Cascade);
            builder.HasOne<NhaCungCap>(x => x.nhacungcap)
                .WithMany(a => a.SanPhams)
                .HasForeignKey(x => x.SupplierID)
                .OnDelete(DeleteBehavior.Cascade);
            builder.HasOne<HangSanPham>(x => x.hangsanpham)
                .WithMany(a => a.SanPhams)
                .HasForeignKey(x => x.BrandID)
                .OnDelete(DeleteBehavior.Cascade);
        }
    }
}
