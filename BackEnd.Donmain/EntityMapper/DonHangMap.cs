﻿using BackEnd.Donmain.Models;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BackEnd.Donmain.EntityMapper
{
    public class DonHangMap : IEntityTypeConfiguration<DonHang>
    {
        public void Configure(EntityTypeBuilder<DonHang> builder)
        {
            builder.ToTable("DonHang")
                .HasKey(x => x.OrderID)
                .HasName("pk_donhangid");
            builder.Property(x => x.OrderID)
                .HasColumnName("OrderID")
                .HasColumnType("INT");
            builder.Property(x => x.OrderDate)
                .HasColumnName("OrderDate")
                .HasColumnType("DATETIME");
            builder.Property(x => x.Status)
                .HasColumnName("Status")
                .HasColumnType("INT");
            builder.Property(x => x.Discount)
                .HasColumnName("Discount")
                .HasColumnType("INT");
            builder.Property(x => x.AccountID)
                .HasColumnName("AccountID")
                .HasColumnType("INT");
            builder.HasOne<KhachHang>(x => x.khachhangs)
                .WithMany(a => a.DonHangs)
                .HasForeignKey(x => x.AccountID)
                .OnDelete(DeleteBehavior.Cascade);
        }
    }
}
