﻿using BackEnd.Donmain.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BackEnd.Donmain.EntityMapper
{
    public class DanhMucTinMap : IEntityTypeConfiguration<DanhMucTin>
    {
        public void Configure(EntityTypeBuilder<DanhMucTin> builder)
        {

            builder.ToTable("DanhMucTin")
                .HasKey(x => x.CatePostID)
                .HasName("pk_danhsachtinid");
            builder.Property(x => x.CatePostID)
                .HasColumnName("CatePostID")
                .HasColumnType("INT");
            builder.Property(x => x.Name)
                .HasColumnName("Name")
                .HasColumnType("NVARCHAR(200)");
            builder.Property(x => x.Status)
                .HasColumnName("Status")
                .HasColumnType("INT");
            builder.Property(x => x.SeoTitle)
                .HasColumnName("SeoTitle")
                .HasColumnType("VARCHAR(200)");
            builder.Property(x => x.ParentID)
                .HasColumnName("ParentID")
                .HasColumnType("INT");
            builder.Property(x => x.CreatedDate)
                .HasColumnName("CreatedDate")
                .HasColumnType("DateTime");
            builder.Property(x => x.UpdatedDate)
                .HasColumnName("UpdatedDate")
                .HasColumnType("DateTime");
        }
    }
}
