﻿using BackEnd.Donmain.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BackEnd.Donmain.EntityMapper
{
    public class NhapHangMap : IEntityTypeConfiguration<NhapHang>
    {
        public void Configure(EntityTypeBuilder<NhapHang> builder)
        {
            builder.ToTable("NhapHang")
                .HasKey(x => x.InvoiceID)
                .HasName("pk_nhahangid");
            builder.Property(x => x.InvoiceID)
                .HasColumnName("InvoiceID")
                .HasColumnType("INT");
            builder.Property(x => x.Status)
                .HasColumnName("Status")
                .HasColumnType("INT");
            builder.Property(x => x.CreateDate)
                .HasColumnName("CreateDate")
                .HasColumnType("DateTime");
            builder.Property(x => x.UpdateDate)
                .HasColumnName("UpdateDate")
                .HasColumnType("DateTime");
        }
    }
}
