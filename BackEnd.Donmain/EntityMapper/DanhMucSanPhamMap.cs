﻿using BackEnd.Donmain.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BackEnd.Donmain.EntityMapper
{
    public class DanhMucSanPhamMap : IEntityTypeConfiguration<DanhMucSanPham>
    {
        public void Configure(EntityTypeBuilder<DanhMucSanPham> builder)
        {
            builder.ToTable("DanhMucSanPham")
               .HasKey(x => x.CateID)
               .HasName("pk_danhmucsanphamid");
            builder.Property(x => x.CateID)
                .HasColumnName("CateID")
                .HasColumnType("INT");
            builder.Property(x => x.Name)
                .HasColumnName("Name")
                .HasColumnType("NVARCHAR(200)");
            builder.Property(x => x.Status)
                .HasColumnName("Status")
                .HasColumnType("INT");
            builder.Property(x => x.SeoTitle)
                .HasColumnName("SeoTitle")
                .HasColumnType("VARCHAR(200)");
            builder.Property(x => x.ParentID)
                .HasColumnName("ParentID")
                .HasColumnType("INT");
            builder.Property(x => x.CreatedDate)
                .HasColumnName("CreatedDate")
                .HasColumnType("DateTime");
            builder.Property(x => x.UpdatedDate)
                .HasColumnName("UpdateDate")
                .HasColumnType("DateTime");
        }
    }
}
