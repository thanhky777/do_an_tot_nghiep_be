﻿using BackEnd.Donmain.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BackEnd.Donmain.EntityMapper
{
    public class ChiTietNhapHangMap : IEntityTypeConfiguration<ChiTietNhapHang>
    {
        public void Configure(EntityTypeBuilder<ChiTietNhapHang> builder)
        {
            builder.ToTable("ChiTietNhapHang")
                .HasKey(x => x.InvoiceDetailID)
                .HasName("pk_chitietnhaphangid");
            builder.Property(x => x.InvoiceDetailID)
                .HasColumnName("InvoiceDetailID")
                .HasColumnType("INT");
            builder.Property(x => x.ProductID)
                .HasColumnName("ProductID")
                .HasColumnType("INT");
            builder.Property(x => x.InvoiceID)
                .HasColumnName("InvoiceID")
                .HasColumnType("INT");
            builder.Property(x => x.ProductName)
                .HasColumnName("ProductName")
                .HasColumnType("NVARCHAR(200)");
            builder.Property(x => x.Quantity)
                .HasColumnName("Quantity")
                .HasColumnType("INT");
            builder.Property(x => x.Price)
                .HasColumnName("Price")
                .HasColumnType("INT");
            builder.HasOne<SanPham>(x => x.sanpham)
                .WithMany(a => a.ChiTietNhapHang)
                .HasForeignKey(x => x.ProductID)
                .OnDelete(DeleteBehavior.Cascade);
            builder.HasOne<NhapHang>(x => x.nhaphang)
                .WithMany(a => a.ChiTietNhapHang)
                .HasForeignKey(x => x.InvoiceID)
                .OnDelete(DeleteBehavior.Cascade);
        }
    }
}
