﻿using BackEnd.Donmain.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BackEnd.Donmain.EntityMapper
{
    public class BinhLuanSanPhamMap : IEntityTypeConfiguration<BinhLuanSanPham>
    {
        public void Configure(EntityTypeBuilder<BinhLuanSanPham> builder)
        {
            builder.ToTable("BinhLuanSanPham")
                .HasKey(x => x.CommentID)
                .HasName("pk_binhluansanphamid");
            builder.Property(x => x.CommentID)
                .HasColumnName("CommentID")
                .HasColumnType("INT");
            builder.Property(x => x.Name)
                .HasColumnName("Name")
                .HasColumnType("NVARCHAR(200)");
            builder.Property(x => x.Email)
                .HasColumnName("Email")
                .HasColumnType("NVARCHAR(200)");
            builder.Property(x => x.Detail)
                .HasColumnName("Detail")
                .HasColumnType("NVARCHAR(200)");
            builder.Property(x => x.Status)
                .HasColumnName("Status")
                .HasColumnType("INT");
            builder.Property(x => x.CreatedDate)
                .HasColumnName("CreatedDate")
                .HasColumnType("DateTime");
            builder.Property(x => x.UpdatedDate)
                .HasColumnName("UpdatedDate")
                .HasColumnType("DateTime");
            builder.Property(x => x.ProductID)
                .HasColumnName("ProductID")
                .HasColumnType("INT");
            builder.HasOne<SanPham>(x => x.sanpham)
                .WithMany(a => a.BinhLuanSanPham)
                .HasForeignKey(x => x.ProductID)
                .OnDelete(DeleteBehavior.Cascade);
        }
    }
}
