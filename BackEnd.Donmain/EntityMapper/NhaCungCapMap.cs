﻿using BackEnd.Donmain.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BackEnd.Donmain.EntityMapper
{
    public class NhaCungCapMap : IEntityTypeConfiguration<NhaCungCap>
    {
        public void Configure(EntityTypeBuilder<NhaCungCap> builder)
        {
            builder.ToTable("NhaCungCap")
               .HasKey(x => x.SupplierID)
               .HasName("pk_nhacungcapid");
            builder.Property(x => x.SupplierID)
                .HasColumnName("SupplierID")
                .HasColumnType("INT");
            builder.Property(x => x.Name)
                .HasColumnName("Name")
                .HasColumnType("NVARCHAR(200)");
            builder.Property(x => x.Email)
                .HasColumnName("Email")
                .HasColumnType("VARCHAR(200)");
            builder.Property(x => x.Phone)
                .HasColumnName("Phone")
                .HasColumnType("INT");
            builder.Property(x => x.Address)
                .HasColumnName("Address")
                .HasColumnType("NVARCHAR(200)");
        }
    }
}
