﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BackEnd.Repository
{
    public interface ISanPhamRepository<T> : IRepository<T> where T : class
    {
        Task<IEnumerable<T>> TimKiemTheoDanhMucSanPham(int cateID);
        Task<IEnumerable<T>> TimKiemTheoHangSanPham(int brandID);
        Task<IEnumerable<T>> SapXepSanPhamAZTenSP();
        Task<IEnumerable<T>> SapXepSanPhamZATenSP();
        Task<IEnumerable<T>> ListSanPhamTrangIndex();
    }
}
