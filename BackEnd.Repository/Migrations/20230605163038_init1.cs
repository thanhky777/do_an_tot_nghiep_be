﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace BackEnd.Repository.Migrations
{
    public partial class init1 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "DanhMucSanPham",
                columns: table => new
                {
                    CateID = table.Column<int>(type: "INT", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "NVARCHAR(200)", nullable: true),
                    Status = table.Column<int>(type: "INT", nullable: false),
                    SeoTitle = table.Column<string>(type: "VARCHAR(200)", nullable: true),
                    ParentID = table.Column<int>(type: "INT", nullable: false),
                    CreatedDate = table.Column<DateTime>(type: "DateTime", nullable: true),
                    UpdateDate = table.Column<DateTime>(type: "DateTime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_danhmucsanphamid", x => x.CateID);
                });

            migrationBuilder.CreateTable(
                name: "DanhMucTin",
                columns: table => new
                {
                    CatePostID = table.Column<int>(type: "INT", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "NVARCHAR(200)", nullable: true),
                    Status = table.Column<int>(type: "INT", nullable: false),
                    SeoTitle = table.Column<string>(type: "VARCHAR(200)", nullable: true),
                    ParentID = table.Column<int>(type: "INT", nullable: false),
                    CreatedDate = table.Column<DateTime>(type: "DateTime", nullable: false),
                    UpdatedDate = table.Column<DateTime>(type: "DateTime", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_danhsachtinid", x => x.CatePostID);
                });

            migrationBuilder.CreateTable(
                name: "GioHang",
                columns: table => new
                {
                    CartID = table.Column<int>(type: "INT", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    AccountID = table.Column<int>(type: "INT", nullable: false),
                    Image = table.Column<string>(type: "NVARCHAR(MAX)", nullable: true),
                    ProductName = table.Column<string>(type: "NVARCHAR(200)", nullable: true),
                    Quantity = table.Column<int>(type: "INT", nullable: false),
                    Price = table.Column<int>(type: "INT", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_giohangid", x => x.CartID);
                });

            migrationBuilder.CreateTable(
                name: "HangSanPham",
                columns: table => new
                {
                    BrandID = table.Column<int>(type: "INT", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "NVARCHAR(200)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_hangsanphamid", x => x.BrandID);
                });

            migrationBuilder.CreateTable(
                name: "KhachHang",
                columns: table => new
                {
                    CommentID = table.Column<int>(type: "INT", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "NVARCHAR(200)", nullable: true),
                    Password = table.Column<string>(type: "NVARCHAR(200)", nullable: true),
                    Email = table.Column<string>(type: "NVARCHAR(200)", nullable: true),
                    Phone = table.Column<string>(type: "VARCHAR(15)", nullable: true),
                    Sex = table.Column<int>(type: "int", nullable: true),
                    Age = table.Column<int>(type: "INT", nullable: true),
                    Avatar = table.Column<string>(type: "NVARCHAR(1000)", nullable: true),
                    Address = table.Column<string>(type: "NVARCHAR(200)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_khachhangid", x => x.CommentID);
                });

            migrationBuilder.CreateTable(
                name: "Menu",
                columns: table => new
                {
                    MenuID = table.Column<int>(type: "INT", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "NVARCHAR(200)", nullable: true),
                    Link = table.Column<string>(type: "VARCHAR(500)", nullable: true),
                    Description = table.Column<string>(type: "NVARCHAR(500)", nullable: true),
                    Status = table.Column<int>(type: "INT", nullable: false),
                    Position = table.Column<int>(type: "INT", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_menuid", x => x.MenuID);
                });

            migrationBuilder.CreateTable(
                name: "NhaCungCap",
                columns: table => new
                {
                    SupplierID = table.Column<int>(type: "INT", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "NVARCHAR(200)", nullable: true),
                    Email = table.Column<string>(type: "VARCHAR(200)", nullable: true),
                    Phone = table.Column<int>(type: "INT", nullable: false),
                    Address = table.Column<string>(type: "NVARCHAR(200)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_nhacungcapid", x => x.SupplierID);
                });

            migrationBuilder.CreateTable(
                name: "NhapHang",
                columns: table => new
                {
                    InvoiceID = table.Column<int>(type: "INT", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Status = table.Column<int>(type: "INT", nullable: false),
                    CreateDate = table.Column<DateTime>(type: "DateTime", nullable: true),
                    UpdateDate = table.Column<DateTime>(type: "DateTime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_nhahangid", x => x.InvoiceID);
                });

            migrationBuilder.CreateTable(
                name: "Slide",
                columns: table => new
                {
                    SlideID = table.Column<int>(type: "INT", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "NVARCHAR(200)", nullable: true),
                    Image = table.Column<string>(type: "VARCHAR(500)", nullable: true),
                    Status = table.Column<int>(type: "INT", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_slideid", x => x.SlideID);
                });

            migrationBuilder.CreateTable(
                name: "Tin",
                columns: table => new
                {
                    PostID = table.Column<int>(type: "INT", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "NVARCHAR(200)", nullable: true),
                    SeoTitle = table.Column<string>(type: "VARCHAR(200)", nullable: true),
                    Status = table.Column<int>(type: "INT", nullable: false),
                    Image = table.Column<string>(type: "NVARCHAR(500)", nullable: true),
                    Hot = table.Column<DateTime>(type: "DATETIME", nullable: false),
                    Description = table.Column<string>(type: "NVARCHAR(500)", nullable: true),
                    Detail = table.Column<string>(type: "NVARCHAR(MAX)", nullable: true),
                    CreatedDate = table.Column<DateTime>(type: "DateTime", nullable: false),
                    UpdatedDate = table.Column<DateTime>(type: "DateTime", nullable: false),
                    CatePostID = table.Column<int>(type: "INT", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_tinid", x => x.PostID);
                    table.ForeignKey(
                        name: "FK_Tin_DanhMucTin_CatePostID",
                        column: x => x.CatePostID,
                        principalTable: "DanhMucTin",
                        principalColumn: "CatePostID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "DonHang",
                columns: table => new
                {
                    OrderID = table.Column<int>(type: "INT", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    OrderDate = table.Column<DateTime>(type: "DATETIME", nullable: false),
                    Status = table.Column<int>(type: "INT", nullable: false),
                    Discount = table.Column<int>(type: "INT", nullable: false),
                    AccountID = table.Column<int>(type: "INT", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_donhangid", x => x.OrderID);
                    table.ForeignKey(
                        name: "FK_DonHang_KhachHang_AccountID",
                        column: x => x.AccountID,
                        principalTable: "KhachHang",
                        principalColumn: "CommentID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "SanPham",
                columns: table => new
                {
                    ProductID = table.Column<int>(type: "INT", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "NVARCHAR(200)", nullable: true),
                    SeoTitle = table.Column<string>(type: "VARCHAR(200)", nullable: true),
                    Status = table.Column<int>(type: "INT", nullable: false),
                    Image = table.Column<string>(type: "NVARCHAR(500)", nullable: true),
                    ListImages = table.Column<string>(type: "VARCHAR(1000)", nullable: true),
                    Price = table.Column<int>(type: "INT", nullable: false),
                    PromotionPrice = table.Column<int>(type: "INT", nullable: false),
                    Quantity = table.Column<int>(type: "INT", nullable: false),
                    Warranty = table.Column<int>(type: "INT", nullable: false),
                    Hot = table.Column<DateTime>(type: "DateTime", nullable: false),
                    Description = table.Column<string>(type: "NVARCHAR(500)", nullable: true),
                    Detail = table.Column<string>(type: "NVARCHAR(MAX)", nullable: true),
                    CreateDate = table.Column<DateTime>(type: "DateTime", nullable: false),
                    UpdateDate = table.Column<DateTime>(type: "DateTime", nullable: false),
                    CateID = table.Column<int>(type: "INT", nullable: true),
                    BrandID = table.Column<int>(type: "INT", nullable: true),
                    SupplierID = table.Column<int>(type: "INT", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_sanphamid", x => x.ProductID);
                    table.ForeignKey(
                        name: "FK_SanPham_DanhMucSanPham_CateID",
                        column: x => x.CateID,
                        principalTable: "DanhMucSanPham",
                        principalColumn: "CateID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_SanPham_HangSanPham_BrandID",
                        column: x => x.BrandID,
                        principalTable: "HangSanPham",
                        principalColumn: "BrandID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_SanPham_NhaCungCap_SupplierID",
                        column: x => x.SupplierID,
                        principalTable: "NhaCungCap",
                        principalColumn: "SupplierID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "BinhLuanTin",
                columns: table => new
                {
                    CommentID = table.Column<int>(type: "INT", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "NVARCHAR(200)", nullable: true),
                    Email = table.Column<string>(type: "VARCHAR(200)", nullable: true),
                    Detail = table.Column<string>(type: "NVARCHAR(500)", nullable: true),
                    Status = table.Column<int>(type: "INT", nullable: false),
                    CreatedDate = table.Column<DateTime>(type: "DateTime", nullable: false),
                    UpdatedDate = table.Column<DateTime>(type: "DateTime", nullable: false),
                    PostID = table.Column<int>(type: "INT", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_binhluantinid", x => x.CommentID);
                    table.ForeignKey(
                        name: "FK_BinhLuanTin_Tin_PostID",
                        column: x => x.PostID,
                        principalTable: "Tin",
                        principalColumn: "PostID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "BinhLuanSanPham",
                columns: table => new
                {
                    CommentID = table.Column<int>(type: "INT", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "NVARCHAR(200)", nullable: true),
                    Email = table.Column<string>(type: "NVARCHAR(200)", nullable: true),
                    Detail = table.Column<string>(type: "NVARCHAR(200)", nullable: true),
                    Status = table.Column<int>(type: "INT", nullable: false),
                    CreatedDate = table.Column<DateTime>(type: "DateTime", nullable: false),
                    UpdatedDate = table.Column<DateTime>(type: "DateTime", nullable: false),
                    ProductID = table.Column<int>(type: "INT", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_binhluansanphamid", x => x.CommentID);
                    table.ForeignKey(
                        name: "FK_BinhLuanSanPham_SanPham_ProductID",
                        column: x => x.ProductID,
                        principalTable: "SanPham",
                        principalColumn: "ProductID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ChiTietDonHang",
                columns: table => new
                {
                    OrderDetailID = table.Column<int>(type: "INT", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    OrderID = table.Column<int>(type: "INT", nullable: false),
                    ProductID = table.Column<int>(type: "INT", nullable: false),
                    ProductName = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Price = table.Column<int>(type: "INT", nullable: false),
                    Quantity = table.Column<int>(type: "INT", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_chitietdonhangid", x => x.OrderDetailID);
                    table.ForeignKey(
                        name: "FK_ChiTietDonHang_DonHang_OrderID",
                        column: x => x.OrderID,
                        principalTable: "DonHang",
                        principalColumn: "OrderID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ChiTietDonHang_SanPham_ProductID",
                        column: x => x.ProductID,
                        principalTable: "SanPham",
                        principalColumn: "ProductID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ChiTietNhapHang",
                columns: table => new
                {
                    InvoiceDetailID = table.Column<int>(type: "INT", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    InvoiceID = table.Column<int>(type: "INT", nullable: false),
                    ProductID = table.Column<int>(type: "INT", nullable: false),
                    ProductName = table.Column<string>(type: "NVARCHAR(200)", nullable: true),
                    Quantity = table.Column<int>(type: "INT", nullable: false),
                    Price = table.Column<int>(type: "INT", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_chitietnhaphangid", x => x.InvoiceDetailID);
                    table.ForeignKey(
                        name: "FK_ChiTietNhapHang_NhapHang_InvoiceID",
                        column: x => x.InvoiceID,
                        principalTable: "NhapHang",
                        principalColumn: "InvoiceID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ChiTietNhapHang_SanPham_ProductID",
                        column: x => x.ProductID,
                        principalTable: "SanPham",
                        principalColumn: "ProductID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_BinhLuanSanPham_ProductID",
                table: "BinhLuanSanPham",
                column: "ProductID");

            migrationBuilder.CreateIndex(
                name: "IX_BinhLuanTin_PostID",
                table: "BinhLuanTin",
                column: "PostID");

            migrationBuilder.CreateIndex(
                name: "IX_ChiTietDonHang_OrderID",
                table: "ChiTietDonHang",
                column: "OrderID");

            migrationBuilder.CreateIndex(
                name: "IX_ChiTietDonHang_ProductID",
                table: "ChiTietDonHang",
                column: "ProductID");

            migrationBuilder.CreateIndex(
                name: "IX_ChiTietNhapHang_InvoiceID",
                table: "ChiTietNhapHang",
                column: "InvoiceID");

            migrationBuilder.CreateIndex(
                name: "IX_ChiTietNhapHang_ProductID",
                table: "ChiTietNhapHang",
                column: "ProductID");

            migrationBuilder.CreateIndex(
                name: "IX_DonHang_AccountID",
                table: "DonHang",
                column: "AccountID");

            migrationBuilder.CreateIndex(
                name: "IX_SanPham_BrandID",
                table: "SanPham",
                column: "BrandID");

            migrationBuilder.CreateIndex(
                name: "IX_SanPham_CateID",
                table: "SanPham",
                column: "CateID");

            migrationBuilder.CreateIndex(
                name: "IX_SanPham_SupplierID",
                table: "SanPham",
                column: "SupplierID");

            migrationBuilder.CreateIndex(
                name: "IX_Tin_CatePostID",
                table: "Tin",
                column: "CatePostID");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "BinhLuanSanPham");

            migrationBuilder.DropTable(
                name: "BinhLuanTin");

            migrationBuilder.DropTable(
                name: "ChiTietDonHang");

            migrationBuilder.DropTable(
                name: "ChiTietNhapHang");

            migrationBuilder.DropTable(
                name: "GioHang");

            migrationBuilder.DropTable(
                name: "Menu");

            migrationBuilder.DropTable(
                name: "Slide");

            migrationBuilder.DropTable(
                name: "Tin");

            migrationBuilder.DropTable(
                name: "DonHang");

            migrationBuilder.DropTable(
                name: "NhapHang");

            migrationBuilder.DropTable(
                name: "SanPham");

            migrationBuilder.DropTable(
                name: "DanhMucTin");

            migrationBuilder.DropTable(
                name: "KhachHang");

            migrationBuilder.DropTable(
                name: "DanhMucSanPham");

            migrationBuilder.DropTable(
                name: "HangSanPham");

            migrationBuilder.DropTable(
                name: "NhaCungCap");
        }
    }
}
