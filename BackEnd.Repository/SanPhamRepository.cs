﻿using BackEnd.Donmain.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BackEnd.Repository
{
    public class SanPhamRepository<SanPham> : ISanPhamRepository<SanPham> where SanPham : class
    {
        private readonly WebAPIDBContext _context;
        private DbSet<SanPham> entities;
        public SanPhamRepository(WebAPIDBContext context)
        {
            _context = context;
            entities = _context.Set<SanPham>();
        }
        public async Task<SanPham> Create(SanPham entity)
        {
            if (entity == null)
            {
                throw new NotImplementedException();
            }
            await entities.AddAsync(entity);
            await _context.SaveChangesAsync();
            return entity;
        }

        public async Task<SanPham> Delete(SanPham entity)
        {
            entities.Remove(entity);
            await _context.SaveChangesAsync();
            return entity;
        }

        public async Task<IEnumerable<SanPham>> GetAll()
        {
            return await entities.ToListAsync();
        }

        public async Task<SanPham> GetById(int id)
        {
            return await entities.FindAsync(id);
        }

        public async Task<IEnumerable<SanPham>> SapXepSanPhamAZTenSP()
        {
            var list = _context.SanPham.Where(a => a.Status == 0).OrderBy(a => a.Name).ToListAsync();
            return (IEnumerable<SanPham>)await list;
        }

        public async Task<IEnumerable<SanPham>> SapXepSanPhamZATenSP()
        {
            var list = _context.SanPham.Where(a =>a.Status == 0).OrderByDescending(a => a.Name).ToListAsync();
            return (IEnumerable<SanPham>)await list;
        }

        public async Task<IEnumerable<SanPham>> TimKiemTheoDanhMucSanPham(int cateID)
        {
            var list = _context.SanPham.Where(a => a.CateID.Equals(cateID) && a.Status == 0).ToListAsync();
            return (IEnumerable<SanPham>)await list;
        }

        public async Task<IEnumerable<SanPham>> TimKiemTheoHangSanPham(int brandID)
        {
            var list = _context.SanPham.Where(a => a.BrandID.Equals(brandID) && a.Status == 0).ToListAsync();
            return (IEnumerable<SanPham>)await list;
        }

        public async Task<SanPham> Update(SanPham entity)
        {
            if (entity == null)
            {
                throw new NotImplementedException("Entity");
            }
            entities.Update(entity);
            await _context.SaveChangesAsync();
            return entity;
        }

        public async Task<IEnumerable<SanPham>> ListSanPhamTrangIndex()
        {
            DateTime currentDate = DateTime.Now.Date;

            var list = await _context.SanPham
                .Where(a => a.Hot <= currentDate && a.Status == 0)
                .OrderByDescending(a => a.Hot)
                .Take(9)
                .OrderBy(a => a.Name)
                .ToListAsync();
            return (IEnumerable<SanPham>) list;
        }
    }
}
