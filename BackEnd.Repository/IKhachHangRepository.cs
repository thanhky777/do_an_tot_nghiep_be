﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BackEnd.Repository
{
    public interface IKhachHangRepository<T> : IRepository<T> where T : class
    {
        Task<IEnumerable<T>> Search(string keyword);
    }
}
