﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BackEnd.Repository
{
    public interface IGioHangRepository<T> : IRepository<T> where T : class
    {
        Task<IEnumerable<T>> Search(int keyword);
        Task<IEnumerable<T>> TimKiemIDAccount(int id);
        Task<IEnumerable<T>> TimKiemAccountAndProduct(int accountID,int productID);
        Task<IEnumerable<T>> RemoveRangeAccountAndProduct(int accountID);
    }
}
