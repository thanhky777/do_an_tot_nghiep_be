﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BackEnd.Repository
{
    public interface IChiTietDonHangRespository<T> : IRepository<T> where T : class
    {
        Task<IEnumerable<T>> RemoveListChiTietDonHang(int orderID);
        Task<IEnumerable<T>> ListOrderID(int orderID);
    }
}
