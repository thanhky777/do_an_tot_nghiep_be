﻿using BackEnd.Donmain.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BackEnd.Repository
{
    public class ChiTietDonHangRepository<ChiTietDonHang> : IChiTietDonHangRespository<ChiTietDonHang> where ChiTietDonHang : class
    {
        private readonly WebAPIDBContext _context;
        private DbSet<ChiTietDonHang> entities;
        public ChiTietDonHangRepository(WebAPIDBContext context)
        {
            _context = context;
            entities = _context.Set<ChiTietDonHang>();
        }
        public async Task<ChiTietDonHang> Create(ChiTietDonHang entity)
        {
            if (entity == null)
            {
                throw new NotImplementedException();
            }
            await entities.AddAsync(entity);
            await _context.SaveChangesAsync();
            return entity;
        }

        public async Task<ChiTietDonHang> Delete(ChiTietDonHang entity)
        {
            entities.Remove(entity);
            await _context.SaveChangesAsync();
            return entity;
        }

        public async Task<IEnumerable<ChiTietDonHang>> GetAll()
        {
            return await entities.ToListAsync();
        }

        public async Task<ChiTietDonHang> GetById(int id)
        {
            return await entities.FindAsync(id);
        }

        public async Task<IEnumerable<ChiTietDonHang>> RemoveListChiTietDonHang(int orderID)
        {
            var listRemove = await _context.ChiTietDonHang.Where(a=>a.OrderID == orderID).ToListAsync();
            _context.ChiTietDonHang.RemoveRange(listRemove);
            await _context.SaveChangesAsync();
            return (IEnumerable<ChiTietDonHang>)listRemove;
        }
        public async Task<IEnumerable<ChiTietDonHang>> ListOrderID(int orderID)
        {
            var listSearch = _context.ChiTietDonHang.Where(a => a.OrderID.Equals(orderID)).ToListAsync();
            return (IEnumerable<ChiTietDonHang>)await listSearch;
        }


        public async Task<ChiTietDonHang> Update(ChiTietDonHang entity)
        {
            if (entity == null)
            {
                throw new NotImplementedException("Entity");
            }
            entities.Update(entity);
            await _context.SaveChangesAsync();
            return entity;
        }
    }
}
