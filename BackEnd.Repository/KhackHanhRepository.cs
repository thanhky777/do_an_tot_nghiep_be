﻿using BackEnd.Donmain.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;

namespace BackEnd.Repository
{
    public class KhackHanhRepository<KhachHang> : IKhachHangRepository<KhachHang> where KhachHang : class
    {
        private readonly WebAPIDBContext _context;

        private DbSet<KhachHang> entities;

        public KhackHanhRepository(WebAPIDBContext context)
        {
            _context = context;
            entities = _context.Set<KhachHang>();
        }
        public async Task<IEnumerable<KhachHang>> GetAll()
        {
            return await entities.ToListAsync();
        }
        public async Task<KhachHang> Create(KhachHang entity)
        {
            if (entity == null)
            {
                throw new NotImplementedException();
            }
            await entities.AddAsync(entity);
            await _context.SaveChangesAsync();
            return entity;
        }
        public async Task<KhachHang> Update(KhachHang entity)
        {
            if (entity == null)
            {
                throw new NotImplementedException("Entity");
            }
            entities.Update(entity);
            await _context.SaveChangesAsync();
            return entity;
        }
        public async Task<KhachHang> Delete(KhachHang entity)
        {
            entities.Remove(entity);
            await _context.SaveChangesAsync();
            return entity;
        }
        public async Task<KhachHang> GetById(int id)
        {
            return await entities.FindAsync(id);
        }
        public async Task<IEnumerable<KhachHang>> Search(string keyword)
        {
            var listSearch = _context.KhachHang.Where(a => a.Email.Equals(keyword)).ToListAsync();
            return (IEnumerable<KhachHang>) await listSearch;
        }
    }
}
