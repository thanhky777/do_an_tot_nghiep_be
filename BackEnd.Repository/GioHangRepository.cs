﻿using BackEnd.Donmain.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BackEnd.Repository
{
    public class GioHangRepository<GioHang> : IGioHangRepository<GioHang> where GioHang : class
    {
        private readonly WebAPIDBContext _context;
        private DbSet<GioHang> entities;

        public GioHangRepository(WebAPIDBContext context)
        {
            _context = context;
            entities = _context.Set<GioHang>();
        }
        public async Task<GioHang> Create(GioHang entity)
        {
            if (entity == null)
            {
                throw new NotImplementedException();
            }
            await entities.AddAsync(entity);
            await _context.SaveChangesAsync();
            return entity;
        }

        public async Task<GioHang> Delete(GioHang entity)
        {
            entities.Remove(entity);
            await _context.SaveChangesAsync();
            return entity;
        }

        public async Task<IEnumerable<GioHang>> GetAll()
        {
            return await entities.ToListAsync();
        }

        public async Task<GioHang> GetById(int id)
        {
            return await entities.FindAsync(id);
        }

        public async Task<IEnumerable<GioHang>> Search(int keyword)
        {
            var listSearch = _context.GioHang.Where(a=>a.ProductID.Equals(keyword)).ToListAsync();
            return (IEnumerable<GioHang>)await listSearch;
        }

        public async Task<IEnumerable<GioHang>> TimKiemIDAccount(int id)
        {
            var listSearch = _context.GioHang.Where(a => a.AccountID.Equals(id)).ToListAsync();
            return (IEnumerable<GioHang>)await listSearch;
        }

        public async Task<IEnumerable<GioHang>> TimKiemAccountAndProduct(int accountID, int productID)
        {
            var listSearch = _context.GioHang.Where(a => a.AccountID == accountID && a.ProductID == productID).ToListAsync();
            return (IEnumerable<GioHang>)await listSearch;
        }

        public async Task<IEnumerable<GioHang>> RemoveRangeAccountAndProduct(int accountID)
        {
            var listRemove = await _context.GioHang.Where(a => a.AccountID == accountID).ToListAsync();
            _context.GioHang.RemoveRange(listRemove);
            await _context.SaveChangesAsync();
            return (IEnumerable<GioHang>) listRemove;
        }


        public async Task<GioHang> Update(GioHang entity)
        {
            if (entity == null)
            {
                throw new NotImplementedException("Entity");
            }
            entities.Update(entity);
            await _context.SaveChangesAsync();
            return entity;
        }
    }
}
