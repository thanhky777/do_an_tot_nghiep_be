﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BackEnd.Repository
{
    public class Repository<T> : IRepository<T> where T : class
    {
        private readonly WebAPIDBContext _context;

        private DbSet<T> entities;

        public Repository(WebAPIDBContext context)
        {
            _context = context;
            entities = _context.Set<T>();
        }
        public async Task<IEnumerable<T>> GetAll()
        {
            return await entities.ToListAsync();
        }
        public async Task<T> Create(T entity)
        {
            if (entity == null)
            {
                throw new NotImplementedException();
            }
            await entities.AddAsync(entity);
            await _context.SaveChangesAsync();
            return entity;
        }
        public async Task<T> Update(T entity)
        {
            if (entity == null)
            {
                throw new NotImplementedException("Entity");
            }
            entities.Update(entity);
            await _context.SaveChangesAsync();
            return entity;
        }
        public async Task<T> Delete(T entity)
        {
            entities.Remove(entity);
            await _context.SaveChangesAsync();
            return entity;
        }
        public async Task<T> GetById(int id)
        {
            return await entities.FindAsync(id);
        }
    }
}
