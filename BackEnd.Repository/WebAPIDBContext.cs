﻿using BackEnd.Donmain.EntityMapper;
using BackEnd.Donmain.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;

namespace BackEnd.Repository
{
    public class WebAPIDBContext: DbContext
    {
        public WebAPIDBContext(DbContextOptions<WebAPIDBContext> options) : base(options) 
        {
        
        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new DanhMucSanPhamMap());
            modelBuilder.ApplyConfiguration(new NhaCungCapMap());
            modelBuilder.ApplyConfiguration(new HangSanPhamMap());
            modelBuilder.ApplyConfiguration(new SanPhamMap());
            modelBuilder.ApplyConfiguration(new BinhLuanSanPhamMap());
            modelBuilder.ApplyConfiguration(new NhapHangMap());
            modelBuilder.ApplyConfiguration(new ChiTietNhapHangMap());
            modelBuilder.ApplyConfiguration(new KhachHangMap());
            modelBuilder.ApplyConfiguration(new DonHangMap());
            modelBuilder.ApplyConfiguration(new ChiTietDonHangMap());
            modelBuilder.ApplyConfiguration(new DanhMucTinMap());
            modelBuilder.ApplyConfiguration(new BinhLuanTinMap());
            modelBuilder.ApplyConfiguration(new TinMap());
            modelBuilder.ApplyConfiguration(new MenuMap());
            modelBuilder.ApplyConfiguration(new SlideMap());
            modelBuilder.ApplyConfiguration(new GioHangMap());
            base.OnModelCreating(modelBuilder);
        }

        public DbSet<KhachHang> KhachHang { get; set; }
        public DbSet<GioHang> GioHang { get; set; }
        public DbSet<ChiTietDonHang> ChiTietDonHang { get; set; }
        public DbSet<SanPham> SanPham { get; set;}
    }
}
