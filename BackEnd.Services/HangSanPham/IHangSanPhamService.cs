﻿using BackEnd.Donmain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BackEnd.Services
{
    public interface IHangSanPhamService
    {
        Task<IEnumerable<HangSanPham>> GetList();
        Task<HangSanPham> GetById(int id);
        Task Create(HangSanPham hangsanpham);
        Task<bool> Update(HangSanPham hangsanpham);
        Task<bool> Delete(int id);
    }
}
