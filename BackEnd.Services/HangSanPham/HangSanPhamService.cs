﻿using BackEnd.Donmain.Models;
using BackEnd.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BackEnd.Services
{
    public class HangSanPhamService : IHangSanPhamService
    {
        private readonly IRepository<HangSanPham> _repository;
        public HangSanPhamService(IRepository<HangSanPham> repository)
        {
            _repository = repository;
        }
        public async Task Create(HangSanPham hangsanpham)
        {
            await _repository.Create(hangsanpham);
        }
        public async Task<bool> Delete(int id)
        {
            var hangsanpham = await _repository.GetById(id);
            if (hangsanpham != null)
            {
                await _repository.Delete(hangsanpham);
            }
            return true;
        }

        public async Task<HangSanPham> GetById(int id)
        {
            return await _repository.GetById(id);
        }

        public async Task<IEnumerable<HangSanPham>> GetList()
        {
            return await _repository.GetAll();
        }

        public async Task<bool> Update(HangSanPham hangsanpham)
        {
            await _repository.Update(hangsanpham);
            return true;
        }
    }
}
