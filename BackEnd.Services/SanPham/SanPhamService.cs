﻿using BackEnd.Donmain.Models;
using BackEnd.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BackEnd.Services
{
    public class SanPhamService : ISanPhamService
    {
        private readonly ISanPhamRepository<SanPham> _repository;
        public SanPhamService(ISanPhamRepository<SanPham> repository)
        {
            _repository = repository;
        }
        public async Task Create(SanPham sanpham)
        {
            await _repository.Create(sanpham);
        }

        public async Task<bool> Delete(int id)
        {
            var sanpham = await _repository.GetById(id);
            if (sanpham != null)
            {
                await _repository.Delete(sanpham);
            }
            return true;
        }

        public async Task<SanPham> GetById(int id)
        {
            return await _repository.GetById(id);
        }

        public async Task<IEnumerable<SanPham>> GetList()
        {
            return await _repository.GetAll();
        }

        public async Task<bool> Update(SanPham sanpham)
        {
            await _repository.Update(sanpham);
            return true;
        }

        public async Task<IEnumerable<SanPham>> TimKiemTheoDanhMucSanPham(int cateID)
        {
            return await _repository.TimKiemTheoDanhMucSanPham(cateID);
        }
        public async Task<IEnumerable<SanPham>> TimKiemTheoHangSanPham(int brandID)
        {
            return await _repository.TimKiemTheoHangSanPham(brandID);
        }

        public async Task<IEnumerable<SanPham>> SapXepSanPhamAZTenSP()
        {
            return await _repository.SapXepSanPhamAZTenSP();
        }

        public async Task<IEnumerable<SanPham>> SapXepSanPhamZATenSP()
        {
            return await _repository.SapXepSanPhamZATenSP();
        }

        public async Task<IEnumerable<SanPham>> ListSanPhamTrangIndex()
        {
            return await _repository.ListSanPhamTrangIndex();
        }

    }
}
