﻿using BackEnd.Donmain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BackEnd.Services
{
    public interface ISanPhamService
    {
        Task<IEnumerable<SanPham>> GetList();
        Task<SanPham> GetById(int id);
        Task Create(SanPham sanpham);
        Task<bool> Update(SanPham sanpham);
        Task<bool> Delete(int id);
        Task<IEnumerable<SanPham>> TimKiemTheoDanhMucSanPham(int cateID);
        Task<IEnumerable<SanPham>> TimKiemTheoHangSanPham(int brandID);
        Task<IEnumerable<SanPham>> SapXepSanPhamAZTenSP();
        Task<IEnumerable<SanPham>> SapXepSanPhamZATenSP();
        Task<IEnumerable<SanPham>> ListSanPhamTrangIndex();
    }
}
