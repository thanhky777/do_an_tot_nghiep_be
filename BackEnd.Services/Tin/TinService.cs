﻿using BackEnd.Donmain.Models;
using BackEnd.Repository;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BackEnd.Services
{
    public class TinService : ITinService
    {
        private readonly IRepository<Tin> _repository;
        private IHostingEnvironment _hostingEnvironment;
        public TinService(IRepository<Tin> repository, IHostingEnvironment hostingEnvironment)
        {
            _repository = repository;
            _hostingEnvironment = hostingEnvironment;
        }
        public async Task Create(Tin tin)
        {
            await _repository.Create(tin);
        }

        public async Task<bool> Delete(int id)
        {
            var tin = await _repository.GetById(id);
            if (tin != null)
            {
                await _repository.Delete(tin);
            }
            return true;
        }

        public async Task<Tin> GetById(int id)
        {
            return await _repository.GetById(id);
        }

        public async Task<IEnumerable<Tin>> GetList()
        {
            return await _repository.GetAll();
        }

        public async Task<bool> Update(Tin tin)
        {
            await _repository.Update(tin);
            return true;
        }

        public async Task UploadFile(IFormFile file)
        {
            var target = Path.Combine(_hostingEnvironment.ContentRootPath, "FolderNewsImage");

            Directory.CreateDirectory(target);

            if (file.Length <= 0)
            {
                return;
            }

            var filePath = Path.Combine(target, file.FileName);

            using (var stream = new FileStream(filePath, FileMode.Create))
            {
                await file.CopyToAsync(stream);
            }
        }
    }
}
