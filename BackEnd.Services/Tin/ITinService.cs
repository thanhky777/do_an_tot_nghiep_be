﻿using BackEnd.Donmain.Models;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BackEnd.Services
{
    public interface ITinService
    {
        Task<IEnumerable<Tin>> GetList();
        Task<Tin> GetById(int id);
        Task Create(Tin tin);
        Task<bool> Update(Tin tin);
        Task<bool> Delete(int id);
        Task UploadFile(IFormFile entity);
    }
}
