﻿using BackEnd.Donmain.Models;
using BackEnd.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BackEnd.Services
{
    public class SlideService : ISlideService
    {
        private readonly IRepository<Slide> _repository;
        public SlideService(IRepository<Slide> repository)
        {
            _repository = repository;
        }
        public async Task Create(Slide slide)
        {
            await _repository.Create(slide);
        }

        public async Task<bool> Delete(int id)
        {
            var slide = await _repository.GetById(id);
            if (slide != null)
            {
                await _repository.Delete(slide);
            }
            return true;
        }

        public async Task<Slide> GetById(int id)
        {
            return await _repository.GetById(id);
        }

        public async Task<IEnumerable<Slide>> GetList()
        {
            return await _repository.GetAll();
        }

        public async Task<bool> Update(Slide slide)
        {
            await _repository.Update(slide);
            return true;
        }
    }
}
