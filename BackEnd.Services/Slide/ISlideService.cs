﻿using BackEnd.Donmain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BackEnd.Services
{
    public interface ISlideService
    {
        Task<IEnumerable<Slide>> GetList();
        Task<Slide> GetById(int id);
        Task Create(Slide slide);
        Task<bool> Update(Slide slide);
        Task<bool> Delete(int id);
    }
}
