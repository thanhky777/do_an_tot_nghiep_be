﻿using BackEnd.Donmain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BackEnd.Services
{
    public interface IKhachHangService
    {
        Task<IEnumerable<KhachHang>> GetList();
        Task<KhachHang> GetById(int id);
        Task Create(KhachHang khachhang);
        Task<bool> Update(KhachHang khachhang);
        Task<bool> Delete(int id);
        Task<IEnumerable<KhachHang>> Search(string keyword);
    }
}
