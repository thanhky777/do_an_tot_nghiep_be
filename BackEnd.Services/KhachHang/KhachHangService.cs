﻿using BackEnd.Donmain.Models;
using BackEnd.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BackEnd.Services
{
    public class KhachHangService : IKhachHangService
    {
        private readonly IKhachHangRepository<KhachHang> _repository;
        public KhachHangService(IKhachHangRepository<KhachHang> repository)
        {
            _repository = repository;
        }
        public async Task Create(KhachHang khachhang)
        {
            await _repository.Create(khachhang);
        }

        public async Task<bool> Delete(int id)
        {
            var khachhang = await _repository.GetById(id);
            if (khachhang != null)
            {
                await _repository.Delete(khachhang);
            }
            return true;
        }

        public async Task<KhachHang> GetById(int id)
        {
            return await _repository.GetById(id);
        }

        public async Task<IEnumerable<KhachHang>> GetList()
        {
            return await _repository.GetAll();
        }

        public async Task<bool> Update(KhachHang khachhang)
        {
            await _repository.Update(khachhang);
            return true;
        }

        public async Task<IEnumerable<KhachHang>> Search(string keyword)
        {
            return await _repository.Search(keyword);
        }
    }
}
