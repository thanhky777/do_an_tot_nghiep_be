﻿using BackEnd.Donmain.Models;
using BackEnd.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BackEnd.Services
{
    public class GioHangService : IGioHangService
    {
        private readonly IGioHangRepository<GioHang> _repository;
        public GioHangService(IGioHangRepository<GioHang> repository)
        {
            _repository = repository;
        }
        public async Task Create(GioHang giohang)
        {
            await _repository.Create(giohang);
        }

        public async Task<bool> Delete(int id)
        {
            var giohang = await _repository.GetById(id);
            if (giohang != null)
            {
                await _repository.Delete(giohang);
            }
            return true;
        }

        public async Task<GioHang> GetById(int id)
        {
            return await _repository.GetById(id);
        }

        public async Task<IEnumerable<GioHang>> GetList()
        {
            return await _repository.GetAll();
        }

        public async Task<bool> Update(GioHang giohang)
        {
            await _repository.Update(giohang);
            return true;
        }

        public async Task<IEnumerable<GioHang>> Search(int keyword)
        {
            return await _repository.Search(keyword);
        }

        public async Task<IEnumerable<GioHang>> TimKiemAccountID(int id)
        {
            return await _repository.TimKiemIDAccount(id);
        }

        public async Task<IEnumerable<GioHang>> TimKiemAccountAndProduct(int accountID, int productID)
        {
            return await _repository.TimKiemAccountAndProduct(accountID, productID);
        }

        public async Task<IEnumerable<GioHang>> RemoveRangeAccountAndProduct(int accountID)
        {
            return await _repository.RemoveRangeAccountAndProduct(accountID);
        }
    }
}
