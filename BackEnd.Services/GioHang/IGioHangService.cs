﻿using BackEnd.Donmain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BackEnd.Services
{
    public interface IGioHangService
    {
        Task<IEnumerable<GioHang>> GetList();
        Task<GioHang> GetById(int id);
        Task Create(GioHang giohang);
        Task<bool> Update(GioHang giohang);
        Task<bool> Delete(int id);
        Task<IEnumerable<GioHang>> Search(int keyword);
        Task<IEnumerable<GioHang>> TimKiemAccountID(int id);
        Task<IEnumerable<GioHang>> TimKiemAccountAndProduct(int accoutID, int productID);
        Task<IEnumerable<GioHang>> RemoveRangeAccountAndProduct(int accoutID);
    }
}
