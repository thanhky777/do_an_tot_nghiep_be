﻿using BackEnd.Donmain.Models;
using BackEnd.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BackEnd.Services
{
    public class NhapHangService : INhapHangService
    {
        private readonly IRepository<NhapHang> _repository;
        public NhapHangService(IRepository<NhapHang> repository)
        {
            _repository = repository;
        }
        public async Task Create(NhapHang nhaphang)
        {
            await _repository.Create(nhaphang);
        }

        public async Task<bool> Delete(int id)
        {
            var nhaphang = await _repository.GetById(id);
            if (nhaphang != null)
            {
                await _repository.Delete(nhaphang);
            }
            return true;
        }

        public async Task<NhapHang> GetById(int id)
        {
            return await _repository.GetById(id);
        }

        public async Task<IEnumerable<NhapHang>> GetList()
        {
            return await _repository.GetAll();
        }

        public async Task<bool> Update(NhapHang nhaphang)
        {
            await _repository.Update(nhaphang);
            return true;
        }
    }
}
