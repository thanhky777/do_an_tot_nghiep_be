﻿using BackEnd.Donmain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BackEnd.Services
{
    public interface INhapHangService
    {
        Task<IEnumerable<NhapHang>> GetList();
        Task<NhapHang> GetById(int id);
        Task Create(NhapHang nhaphang);
        Task<bool> Update(NhapHang nhaphang);
        Task<bool> Delete(int id);
    }
}
