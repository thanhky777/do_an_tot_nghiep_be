﻿using BackEnd.Donmain.Models;
using BackEnd.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BackEnd.Services
{
    public class DanhMucTinService : IDanhMucTinService
    {
        private readonly IRepository<DanhMucTin> _repository;
        public DanhMucTinService(IRepository<DanhMucTin> repository)
        {
            _repository = repository;
        }
        public async Task Create(DanhMucTin danhmuctin)
        {
            await _repository.Create(danhmuctin);
        }

        public async Task<bool> Delete(int id)
        {
            var danhmuctin = await _repository.GetById(id);
            if (danhmuctin != null)
            {
                await _repository.Delete(danhmuctin);
            }
            return true;
        }

        public async Task<DanhMucTin> GetById(int id)
        {
            return await _repository.GetById(id);
        }

        public async Task<IEnumerable<DanhMucTin>> GetList()
        {
            return await _repository.GetAll();
        }

        public async Task<bool> Update(DanhMucTin danhmuctin)
        {
            await _repository.Update(danhmuctin);
            return true;
        }
    }
}
