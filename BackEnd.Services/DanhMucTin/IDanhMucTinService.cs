﻿using BackEnd.Donmain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BackEnd.Services
{
    public interface IDanhMucTinService
    {
        Task<IEnumerable<DanhMucTin>> GetList();
        Task<DanhMucTin> GetById(int id);
        Task Create(DanhMucTin danhmuctin);
        Task<bool> Update(DanhMucTin danhmuctin);
        Task<bool> Delete(int id);
    }
}
