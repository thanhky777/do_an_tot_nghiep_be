﻿using BackEnd.Donmain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BackEnd.Services
{
    public interface IDanhMucSanPhamService
    {
        Task<IEnumerable<DanhMucSanPham>> GetList();
        Task<DanhMucSanPham> GetById(int id);
        Task Create(DanhMucSanPham danhmucsanpham);
        Task<bool> Update(DanhMucSanPham danhmucsanpham);
        Task<bool> Delete(int id);
    }
}
