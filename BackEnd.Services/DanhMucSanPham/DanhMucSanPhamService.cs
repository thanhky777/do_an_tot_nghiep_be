﻿using BackEnd.Donmain.Models;
using BackEnd.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BackEnd.Services
{
    public class DanhMucSanPhamService : IDanhMucSanPhamService
    {
        private readonly IRepository<DanhMucSanPham> _repository;
        public DanhMucSanPhamService(IRepository<DanhMucSanPham> repository)
        {
            _repository = repository;
        }
        public async Task Create(DanhMucSanPham danhmucsanpham)
        {
            await _repository.Create(danhmucsanpham);
        }

        public async Task<bool> Delete(int id)
        {
            var danhmucsanpham = await _repository.GetById(id);
            if (danhmucsanpham != null)
            {
                await _repository.Delete(danhmucsanpham);
            }
            return true;
        }

        public async Task<DanhMucSanPham> GetById(int id)
        {
            return await _repository.GetById(id);
        }

        public async Task<IEnumerable<DanhMucSanPham>> GetList()
        {
            return await _repository.GetAll();
        }

        public async Task<bool> Update(DanhMucSanPham danhmucsanpham)
        {
            await _repository.Update(danhmucsanpham);
            return true;
        }
    }
}
