﻿using BackEnd.Donmain.Models;
using BackEnd.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BackEnd.Services
{
    public class MenuService : IMenuService
    {
        private readonly IRepository<Menu> _repository;
        public MenuService(IRepository<Menu> repository)
        {
            _repository = repository;
        }
        public async Task Create(Menu menu)
        {
            await _repository.Create(menu);
        }

        public async Task<bool> Delete(int id)
        {
            var menu = await _repository.GetById(id);
            if (menu != null)
            {
                await _repository.Delete(menu);
            }
            return true;
        }

        public async Task<Menu> GetById(int id)
        {
            return await _repository.GetById(id);
        }

        public async Task<IEnumerable<Menu>> GetList()
        {
            return await _repository.GetAll();
        }

        public async Task<bool> Update(Menu menu)
        {
            await _repository.Update(menu);
            return true;
        }
    }
}
