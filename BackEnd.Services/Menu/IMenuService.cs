﻿using BackEnd.Donmain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BackEnd.Services
{
    public interface IMenuService
    {
        Task<IEnumerable<Menu>> GetList();
        Task<Menu> GetById(int id);
        Task Create(Menu menu);
        Task<bool> Update(Menu menu);
        Task<bool> Delete(int id);
    }
}

