﻿using BackEnd.Donmain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BackEnd.Services
{
    public interface IBinhLuanTinService
    {
        Task<IEnumerable<BinhLuanTin>> GetList();
        Task<BinhLuanTin> GetById(int id);
        Task Create(BinhLuanTin binhluantin);
        Task<bool> Update(BinhLuanTin binhluantin);
        Task<bool> Delete(int id);
    }
}
