﻿using BackEnd.Donmain.Models;
using BackEnd.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BackEnd.Services
{
    public class BinhLuanTinService : IBinhLuanTinService
    {
        private readonly IRepository<BinhLuanTin> _repository;
        public BinhLuanTinService(IRepository<BinhLuanTin> repository)
        {
            _repository = repository;
        }
        public async Task Create(BinhLuanTin binhluantin)
        {
            await _repository.Create(binhluantin);
        }

        public async Task<bool> Delete(int id)
        {
            var binhluantin = await _repository.GetById(id);
            if (binhluantin != null)
            {
                await _repository.Delete(binhluantin);
            }
            return true;
        }

        public async Task<BinhLuanTin> GetById(int id)
        {
            return await _repository.GetById(id);
        }

        public async Task<IEnumerable<BinhLuanTin>> GetList()
        {
            return await _repository.GetAll();
        }

        public async Task<bool> Update(BinhLuanTin binhluantin)
        {
            await _repository.Update(binhluantin);
            return true;
        }
    }
}
