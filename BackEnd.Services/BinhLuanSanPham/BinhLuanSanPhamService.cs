﻿using BackEnd.Donmain.Models;
using BackEnd.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BackEnd.Services
{
    public class BinhLuanSanPhamService : IBinhLuanSanPhamService
    {
        private readonly IRepository<BinhLuanSanPham> _repository;
        public BinhLuanSanPhamService(IRepository<BinhLuanSanPham> repository)
        {
            _repository = repository;
        }
        public async Task Create(BinhLuanSanPham binhluansanpham)
        {
            await _repository.Create(binhluansanpham);
        }

        public async Task<bool> Delete(int id)
        {
            var blsanpham = await _repository.GetById(id);
            if (blsanpham != null)
            {
                await _repository.Delete(blsanpham);
            }
            return true;
        }

        public async Task<BinhLuanSanPham> GetById(int id)
        {
            return await _repository.GetById(id);
        }

        public async Task<IEnumerable<BinhLuanSanPham>> GetList()
        {
            return await _repository.GetAll();
        }

        public async Task<bool> Update(BinhLuanSanPham binhluansanpham)
        {
            await _repository.Update(binhluansanpham);
            return true;
        }
    }
}
