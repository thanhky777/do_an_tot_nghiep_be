﻿using BackEnd.Donmain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BackEnd.Services
{
    public interface IBinhLuanSanPhamService
    {
        Task<IEnumerable<BinhLuanSanPham>> GetList();
        Task<BinhLuanSanPham> GetById(int id);
        Task Create(BinhLuanSanPham binhluansanpham);
        Task<bool> Update(BinhLuanSanPham binhluansanpham);
        Task<bool> Delete(int id);
    }
}
