﻿using BackEnd.Donmain.Models;
using BackEnd.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BackEnd.Services
{
    public class ChiTietNhapHangService : IChiTietNhapHangService
    {
        private readonly IRepository<ChiTietNhapHang> _repository;
        public ChiTietNhapHangService(IRepository<ChiTietNhapHang> repository)
        {
            _repository = repository;
        }
        public async Task Create(ChiTietNhapHang chitietnhaphang)
        {
            await _repository.Create(chitietnhaphang);
        }

        public async Task<bool> Delete(int id)
        {
            var chitietnhaphang = await _repository.GetById(id);
            if (chitietnhaphang != null)
            {
                await _repository.Delete(chitietnhaphang);
            }
            return true;
        }

        public async Task<ChiTietNhapHang> GetById(int id)
        {
            return await _repository.GetById(id);
        }

        public async Task<IEnumerable<ChiTietNhapHang>> GetList()
        {
            return await _repository.GetAll();
        }

        public async Task<bool> Update(ChiTietNhapHang chitietnhaphang)
        {
            await _repository.Update(chitietnhaphang);
            return true;
        }
    }
}
