﻿using BackEnd.Donmain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BackEnd.Services
{
    public interface IChiTietNhapHangService
    {
        Task<IEnumerable<ChiTietNhapHang>> GetList();
        Task<ChiTietNhapHang> GetById(int id);
        Task Create(ChiTietNhapHang chitietnhaphang);
        Task<bool> Update(ChiTietNhapHang chitietnhaphang);
        Task<bool> Delete(int id);
    }
}
