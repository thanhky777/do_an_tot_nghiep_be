﻿using BackEnd.Donmain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BackEnd.Services
{
    public interface IChiTietDonHangService
    {
        Task<IEnumerable<ChiTietDonHang>> GetList();
        Task<ChiTietDonHang> GetById(int id);
        Task Create(ChiTietDonHang chitietdonhang);
        Task<bool> Update(ChiTietDonHang chitietdonhang);
        Task<bool> Delete(int id);
        Task<IEnumerable<ChiTietDonHang>> RemoveListOrder(int orderID);
        Task<IEnumerable<ChiTietDonHang>> ListOrderID(int orderID);
    }
}
