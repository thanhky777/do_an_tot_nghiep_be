﻿using BackEnd.Donmain.Models;
using BackEnd.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BackEnd.Services
{
    public class ChiTietDonHangService : IChiTietDonHangService
    {
        private readonly IChiTietDonHangRespository<ChiTietDonHang> _repository;
        public ChiTietDonHangService(IChiTietDonHangRespository<ChiTietDonHang> repository)
        {
            _repository = repository;
        }
        public async Task Create(ChiTietDonHang chitietdonhang)
        {
            await _repository.Create(chitietdonhang);
        }
        public async Task<bool> Delete(int id)
        {
            var chitietdonhang = await _repository.GetById(id);
            if (chitietdonhang != null)
            {
                await _repository.Delete(chitietdonhang);
            }
            return true;
        }

        public async Task<ChiTietDonHang> GetById(int id)
        {
            return await _repository.GetById(id);
        }

        public async Task<IEnumerable<ChiTietDonHang>> GetList()
        {
            return await _repository.GetAll();
        }

        public async Task<bool> Update(ChiTietDonHang chitietdonhang)
        {
            await _repository.Update(chitietdonhang);
            return true;
        }

        public async Task<IEnumerable<ChiTietDonHang>> RemoveListOrder(int orderID)
        {
            return await _repository.RemoveListChiTietDonHang(orderID);
        }

        public async Task<IEnumerable<ChiTietDonHang>> ListOrderID(int orderID)
        {
            return await _repository.ListOrderID(orderID);
        }
    }
}
