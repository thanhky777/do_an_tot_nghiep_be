﻿using BackEnd.Donmain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BackEnd.Services
{
    public interface INhaCungCapService
    {
        Task<IEnumerable<NhaCungCap>> GetList();
        Task<NhaCungCap> GetById(int id);
        Task Create(NhaCungCap nhacungcap);
        Task<bool> Update(NhaCungCap nhacungcap);
        Task<bool> Delete(int id);
    }
}
