﻿using BackEnd.Donmain.Models;
using BackEnd.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BackEnd.Services
{
    public class NhaCungCapService : INhaCungCapService
    {
        private readonly IRepository<NhaCungCap> _repository;
        public NhaCungCapService(IRepository<NhaCungCap> repository)
        {
            _repository = repository;
        }
        public async Task Create(NhaCungCap nhacungcap)
        {
            await _repository.Create(nhacungcap);
        }

        public async Task<bool> Delete(int id)
        {
            var nhacungcap = await _repository.GetById(id);
            if (nhacungcap != null)
            {
                await _repository.Delete(nhacungcap);
            }
            return true;
        }

        public async Task<NhaCungCap> GetById(int id)
        {
            return await _repository.GetById(id);
        }

        public async Task<IEnumerable<NhaCungCap>> GetList()
        {
            return await _repository.GetAll();
        }

        public async Task<bool> Update(NhaCungCap nhacungcap)
        {
            await _repository.Update(nhacungcap);
            return true;
        }
    }
}
