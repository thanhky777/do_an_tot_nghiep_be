﻿using BackEnd.Donmain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BackEnd.Services
{
    public interface IDonHangService
    {
        Task<IEnumerable<DonHang>> GetList();
        Task<DonHang> GetById(int id);
        Task Create(DonHang donhang);
        Task<bool> Update(DonHang donhang);
        Task<bool> Delete(int id);
    }
}
