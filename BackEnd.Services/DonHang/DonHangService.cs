﻿using BackEnd.Donmain.Models;
using BackEnd.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BackEnd.Services
{
    public class DonHangService : IDonHangService
    {
        private readonly IRepository<DonHang> _repository;
        public DonHangService(IRepository<DonHang> repository)
        {
            _repository = repository;
        }
        public async Task Create(DonHang donhang)
        {
            await _repository.Create(donhang);
        }

        public async Task<bool> Delete(int id)
        {
            var donhang = await _repository.GetById(id);
            if (donhang != null)
            {
                await _repository.Delete(donhang);
            }
            return true;
        }

        public async Task<DonHang> GetById(int id)
        {
            return await _repository.GetById(id);
        }

        public async Task<IEnumerable<DonHang>> GetList()
        {
            return await _repository.GetAll();
        }

        public async Task<bool> Update(DonHang donhang)
        {
            await _repository.Update(donhang);
            return true;
        }
    }
}
